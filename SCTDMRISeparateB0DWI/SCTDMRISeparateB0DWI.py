# TODO: better import of sct scripts

import sys
import os
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import SimpleITK as sitk
import sitkUtils

#
# SCTDMRISeparateB0DWI
#

class SCTDMRISeparateB0DWI(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "SCT DMRI Separate b0 and dwi"
    self.parent.categories = ["Spinal Cord Toolbox"]
    self.parent.contributors = ["Vignesh Sivan (Sunnybrook)"]
    self.parent.helpText = """
Separate b=0 and DW images from diffusion dataset.

    <p>For more information, visit the <a href="https://sourceforge.net/p/spinalcordtoolbox/wiki/sct_dmri_separate_b0_and_dwi/">spinalcord toolbox documentation</a></p>
    """
    self.parent.acknowledgementText = ""

#
# SCTDMRISeparateB0DWIWidget
#

class SCTDMRISeparateB0DWIWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    #
    # Main Options
    #

    self.statusDict = {}
    self.statusDict[0] = "Status: SCT Separate B0 DWI ran successfully"
    self.statusDict[1] = "Status: Could not load output files from SCT Separate B0 DWI"
    self.statusDict[-1] = "Status: SCT Separate B0 DWI did not run because sct/scripts directory was not found"
    self.statusDict[-2] = "Status: SCT Separate B0 DWI threw some exception"

    mandatoryOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    mandatoryOptionsCollapsibleButton.text = "Mandatory Options"
    self.layout.addWidget(mandatoryOptionsCollapsibleButton)

    mandatoryOptionsLayout = qt.QFormLayout(mandatoryOptionsCollapsibleButton)

    self.diffusionDataSelector = slicer.qMRMLNodeComboBox()
    self.diffusionDataSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.diffusionDataSelector.selectNodeUponCreation = True
    self.diffusionDataSelector.addEnabled = False
    self.diffusionDataSelector.removeEnabled = True
    self.diffusionDataSelector.noneEnabled = True
    self.diffusionDataSelector.showHidden = False
    self.diffusionDataSelector.showChildNodeTypes = False
    self.diffusionDataSelector.setMRMLScene( slicer.mrmlScene )
 
    mandatoryOptionsLayout.addRow("Diffusion data", self.diffusionDataSelector)

    self.bvecsSelector = ctk.ctkPathLineEdit()
    self.bvecsSelector.filters = ctk.ctkPathLineEdit.Files
    mandatoryOptionsLayout.addRow("bvecs file", self.bvecsSelector)

    #
    # Optional Arguments
    #

    optionalArgumentsCollapsibleButton = ctk.ctkCollapsibleButton()
    optionalArgumentsCollapsibleButton.text = "Optional Arguments"
    self.layout.addWidget(optionalArgumentsCollapsibleButton)

    optionalArgumentsLayout = qt.QFormLayout(optionalArgumentsCollapsibleButton)

    self.bvalsSelector = ctk.ctkPathLineEdit()
    self.bvalsSelector.filters = ctk.ctkPathLineEdit.Files
    optionalArgumentsLayout.addRow("b-values file", self.bvalsSelector)

    self.bvalminSelector = ctk.ctkDoubleSpinBox()
    self.bvalminSelector.minimum = 0
    optionalArgumentsLayout.addRow("b-value threshold", self.bvalminSelector)

    self.outputFolderSelector = ctk.ctkPathLineEdit()
    self.outputFolderSelector.filters = ctk.ctkPathLineEdit.Dirs
    optionalArgumentsLayout.addRow("Output folder", self.outputFolderSelector)

    verbosityDict = {0: "No verbosity",
                     1: "Basic verbosity",
                     2: "Extended verbosity"}

    self.verboseSelector = qt.QComboBox()
    self.verboseSelector.addItems(verbosityDict.values())
    self.verboseSelector.setCurrentIndex(1)
    optionalArgumentsLayout.addRow("Verbosity", self.verboseSelector)

    self.averageb0dwiSelector = qt.QCheckBox()
    self.averageb0dwiSelector.setChecked(True)
    optionalArgumentsLayout.addRow("Average b=0 and dwi", self.averageb0dwiSelector)

    self.removeTempFilesSelector = qt.QCheckBox()
    self.removeTempFilesSelector.setChecked(True)
    optionalArgumentsLayout.addRow("Remove temporary files", self.removeTempFilesSelector)

    #
    # Run button
    #

    self.runButton = qt.QPushButton("Run SCT Separate b0 and dwi")
    self.runButton.enabled = False

    self.resetButton = qt.QPushButton("Reset values")

    self.buttonLayout = qt.QHBoxLayout()
    # self.buttonLayout.addWidget(self.resetButton)
    self.buttonLayout.addWidget(self.runButton)

    self.layout.addLayout(self.buttonLayout)

    self.statusLabel = qt.QLabel("Status: ")
    self.layout.addWidget(self.statusLabel)

    #
    # Connections
    #

    self.diffusionDataSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelectDiffusionData)
    self.bvecsSelector.connect("currentPathChanged(const QString&)", self.onSelectBvecs)
    self.runButton.connect('clicked(bool)', self.onRunButton)

  def onSelectDiffusionData(self):
    diffusionData = self.diffusionDataSelector.currentNode()

    if diffusionData:

      diffusionDataPath = diffusionData.GetStorageNode().GetFileName()
      diffusionDataDir = diffusionDataPath[:diffusionDataPath.rfind("/") + 1]

      # check if bvecs in current directory
      if os.path.isfile(diffusionDataDir + "bvecs.txt") and not self.bvecsSelector.currentPath:
        self.bvecsSelector.currentPath = diffusionDataDir + "bvecs.txt"

      # change the output folder to current directory
      if not self.outputFolderSelector.currentPath:
        self.outputFolderSelector.currentPath = diffusionDataDir

      self.runButton.enabled = self.bvecsSelector.currentPath

  def onSelectBvecs(self):
    self.runButton.enabled = self.diffusionDataSelector.currentNode()

  def getParams(self):
    # Append mandatory options
    params = {}
    params["-i "] = self.diffusionDataSelector.currentNode().GetStorageNode().GetFileName()
    params["-bvec"] = self.bvecsSelector.currentPath

    # Append optional options
    if not self.averageb0dwiSelector.isChecked():
      params["-a"] = 0
    if self.bvalsSelector.currentPath:
      params["-bval"] = self.bvalsSelector.currentPath
    if self.bvalminSelector.value != 0:
      params["-bvalmin"] = ""
    if self.outputFolderSelector.currentPath:
      params["-ofolder"] = self.outputFolderSelector.currentPath
    if self.verboseSelector.currentIndex != 1:
      params["-v"] = self.verboseSelector.currentIndex
    if not self.removeTempFilesSelector.isChecked():
      params["-r"] = 0

    return params

  def onRunButton(self):
    logic = SCTDMRISeparateB0DWILogic()
    params = self.getParams()
    statusCode = logic.run(params)
    self.statusLabel.text = self.statusDict[statusCode]

  def cleanup(self):
    pass

#
# SCTDMRISeparateB0DWILogic
#

class SCTDMRISeparateB0DWILogic(ScriptedLoadableModuleLogic):

  def run(self, params_dict):
    """calls sct_utils.run to run sct_dmri_separate_b0_dwi
    Adds sct_scripts to path by looking at bash variables
    Returns status code:
    statusCode == 0  : sct_dmri_separate_b0_dwi ran without any error
    statusCode == 1  : sct_dmri_separate_b0_dwi ran successfully but could not load output
    statusCode == -1 : sct_dmri_separate_b0_dwi did not run because sct/scripts dir was not found
    statusCode == -2 : sct_dmri_separate_b0_dwi ran and threw some exception
    """

    statusCode = 0

    try:

      if self.add_sct_scripts_to_path():

        import sct_utils as sct
        import sct_dmri_separate_b0_and_dwi
        from msct_parser import Parser

        params_str = Parser.dictionary_to_string(sct_dmri_separate_b0_and_dwi.get_parser(), params_dict)

        sct.run("sct_dmri_separate_b0_and_dwi " + params_str)

        # load output nodes

        b0Name = "b0"
        dwiName = "dwi"
        b0_meanName = "b0_mean"
        dwi_meanName = "dwi_mean"

        self.removeNodeIfExists(b0Name)
        self.removeNodeIfExists(b0_meanName)
        self.removeNodeIfExists(dwiName)
        self.removeNodeIfExists(dwi_meanName)

        outputFolder = params_dict["-ofolder"]
        nii = ".nii.gz"

        load_b0, b0 = slicer.util.loadVolume(outputFolder + b0Name + nii, {}, True)
        load_dwi, dwi = slicer.util.loadVolume(outputFolder + dwiName + nii, {}, True)
        load_b0_mean, b0_mean = slicer.util.loadVolume(outputFolder + b0_meanName + nii, {}, True)
        load_dwi_mean, dwi_mean = slicer.util.loadVolume(outputFolder + dwi_meanName + nii, {}, True)

        if not load_b0: statusCode = 1
        else: b0.SetName(b0Name)
        if not load_dwi: statusCode = 1
        else: dwi.SetName(dwiName)
        if not load_b0_mean: statusCode = 1
        else: b0_mean.SetName(b0_meanName)
        if not load_dwi_mean: statusCode = 1
        else:
          dwi_mean.SetName(dwi_meanName)
          # display dwi_mean
          selectionNode = slicer.app.applicationLogic().GetSelectionNode()
          selectionNode.SetReferenceActiveVolumeID(dwi_mean.GetID())

      else:
        statusCode = -1

      # Update view
      applicationLogic = slicer.app.applicationLogic()
      applicationLogic.PropagateVolumeSelection(0)

    except: # Prevent Slicer from crashing
      statusCode = -2
      print(sys.exc_info())

    return statusCode

  def removeNodeIfExists(self, nodeName):
    node = slicer.util.getNode(nodeName)
    if node:
      slicer.mrmlScene.RemoveNode(node)

  def add_sct_scripts_to_path(self):
    '''Finds sct/bin directory in system path and adds sct/scripts to system path variable
    returns True if sct/bin was found in system path, else returns false'''

    paths = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          sct_scripts_path =  sct_bin_path[:sct_bin_path.rfind("/")] + "/scripts"
          sys.path.append(sct_scripts_path)
          return True
      return False
    except KeyError:  # path variable not found
      return False

#
# SCTDMRISeparateB0DWITest
#

class SCTDMRISeparateB0DWITest(ScriptedLoadableModuleTest):

  def setUp(self):
    slicer.mrmlScene.Clear(0)
    sys.path.append("/usr/local/Vignesh/sct/scripts")
    import sct_testing

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_SCTDMRISeparateB0DWI()

  def test_SCTDMRISeparateB0DWI(self):
    """Calls sct_testing to carry out the testing
    """
    import sct_testing
    try:
      sct_testing.main(["-f","sct_separate_b0_and_dwi"])
      self.delayDisplay('Successfully ran sct_testing')
    except:
      self.delayDisplay("Error running sct_testing")
