import sys
import os
import unittest
import vtk, qt, ctk, slicer
import ctk
from slicer.ScriptedLoadableModule import *
import SimpleITK as sitk
import sitkUtils

#
# SCTLabelUtils
#

class SCTLabelUtils(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "SCT Label Utils"
    self.parent.categories = ["Spinal Cord Toolbox"]
    self.parent.dependencies = []
    self.parent.contributors = ["Vignesh Sivan (Sunnybrook)"]
    self.parent.helpText = """
    Utility function for label image.
    <p>For more information, visit the <a href="https://sourceforge.net/p/spinalcordtoolbox/wiki/sct_label_utils/">spinalcordtoolbox documentation</a></p>
    """
    self.parent.acknowledgementText = ""

#
# SCTLabelUtilsWidget
#

class SCTLabelUtilsWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):

    # defaults

    self.crossDefault          = 5
    self.verbosityDefault      = 1
    self.outputImagesDefault   = "labels.nii.gz"

    self.statusDict = {}
    self.statusDict[0] = "Status: SCT Label Utils ran successfully"
    self.statusDict[1] = "Status: Could not load output files from SCT Label Utils"
    self.statusDict[-1] = "Status: SCT Label Utils did not run because sct directory was not found"
    self.statusDict[-2] = "Status: SCT Label Utils threw some exception"

    ScriptedLoadableModuleWidget.setup(self)

    #
    # Options
    #

    OptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    OptionsCollapsibleButton.text = "Mandatory Options"
    self.layout.addWidget(OptionsCollapsibleButton)

    OptionsLayout = qt.QFormLayout(OptionsCollapsibleButton)

    self.inputImageSelector = slicer.qMRMLNodeComboBox()
    self.inputImageSelector.nodeTypes = ["vtkMRMLLabelMapVolumeNode", "vtkMRMLScalarVolumeNode"]
    self.inputImageSelector.selectNodeUponCreation = True
    self.inputImageSelector.addEnabled = False
    self.inputImageSelector.removeEnabled = True
    self.inputImageSelector.noneEnabled = True
    self.inputImageSelector.showHidden = False
    self.inputImageSelector.showChildNodeTypes = False
    self.inputImageSelector.setMRMLScene( slicer.mrmlScene )

    OptionsLayout.addRow("Input image", self.inputImageSelector)

    self.outputImagesDirectory = ctk.ctkPathLineEdit()
    self.outputImagesDirectory.filters = ctk.ctkPathLineEdit.Dirs
    self.outputImagesDirectory.currentPath = os.getcwd() + "/" + self.outputImagesDefault
    OptionsLayout.addRow("Output image(s)", self.outputImagesDirectory)

    #
    # Functions
    #

    generalOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    generalOptionsCollapsibleButton.text = "Processes"
    self.layout.addWidget(generalOptionsCollapsibleButton)

    generalOptionsLayout = qt.QFormLayout(generalOptionsCollapsibleButton)

    self.addSelector = qt.QSpinBox()
    self.addSelector.setRange(-1000,1000)
    generalOptionsLayout.addRow("Add value to all labels", self.addSelector)

    # TODO: in Slicer, one should be able to create labels using the Slicer UI itself

    self.createSelector = LabelsCreationFormWidget("Create labels in new image")
    generalOptionsLayout.addRow(self.createSelector)

    self.createAddSelector = LabelsCreationFormWidget("Create labels in same image")
    generalOptionsLayout.addRow(self.createAddSelector)

    self.createSegSelector = LabelsCreationFormWidget("Create labels along cord segmentation", "z")
    generalOptionsLayout.addRow(self.createSegSelector)

    self.crossSelector = qt.QSpinBox()
    self.crossSelector.value = self.crossDefault
    generalOptionsLayout.addRow("Radius cross (mm)", self.crossSelector)

    self.vertbodySelector = qt.QLineEdit()
    self.vertbodySelector.setPlaceholderText("level1,level2,...")
    generalOptionsLayout.addRow("Vertebral body levels", self.vertbodySelector)

    self.MSEImageSelector = slicer.qMRMLNodeComboBox()
    self.MSEImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.MSEImageSelector.selectNodeUponCreation = True
    self.MSEImageSelector.addEnabled = False
    self.MSEImageSelector.removeEnabled = True
    self.MSEImageSelector.noneEnabled = True
    self.MSEImageSelector.showHidden = False
    self.MSEImageSelector.showChildNodeTypes = False
    self.MSEImageSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Ref. image for MSE computation", self.MSEImageSelector)

    self.removeSelector = slicer.qMRMLNodeComboBox()
    self.removeSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.removeSelector.selectNodeUponCreation = True
    self.removeSelector.addEnabled = False
    self.removeSelector.removeEnabled = True
    self.removeSelector.noneEnabled = True
    self.removeSelector.showHidden = False
    self.removeSelector.showChildNodeTypes = False
    self.removeSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Ref. image for label removal in new image", self.removeSelector)

    self.removesymSelector = slicer.qMRMLNodeComboBox()
    self.removesymSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.removesymSelector.selectNodeUponCreation = True
    self.removesymSelector.addEnabled = False
    self.removesymSelector.removeEnabled = True
    self.removesymSelector.noneEnabled = True
    self.removesymSelector.showHidden = False
    self.removesymSelector.showChildNodeTypes = False
    self.removesymSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Ref. image for label removal in both images",
                                self.removesymSelector)

    self.cubicToPointSelector = qt.QCheckBox()
    generalOptionsLayout.addRow("Compute center-of-mass for each label", self.cubicToPointSelector)

    self.displaySelector = qt.QCheckBox()
    generalOptionsLayout.addRow("Display all labels", self.displaySelector)

    self.incrementSelector = qt.QCheckBox()
    generalOptionsLayout.addRow("Sort labels in incremental order", self.incrementSelector)

    self.vertcontinuousSelector = qt.QCheckBox()
    generalOptionsLayout.addRow("Discrete to continuous labelling", self.vertcontinuousSelector)

    #
    # Run button
    #

    self.runButton = qt.QPushButton("Run SCT Label Utils")
    self.runButton.enabled = False

    self.resetButton = qt.QPushButton("Reset values")

    self.buttonLayout = qt.QHBoxLayout()
    #self.buttonLayout.addWidget(self.resetButton)
    self.buttonLayout.addWidget(self.runButton)

    self.layout.addLayout(self.buttonLayout)

    #
    # Status Label
    #

    self.statusLabel = qt.QLabel("Status: ")
    self.layout.addWidget(self.statusLabel)

    #
    # Connections
    #

    self.inputImageSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelectInputImage)
    self.runButton.connect('clicked(bool)', self.onRunButton)

  def onSelectInputImage(self):
    inputNode = self.inputImageSelector.currentNode()

    if inputNode:
      self.inputPath = inputNode.GetStorageNode().GetFileName()
      outputPath = self.inputPath[:self.inputPath.rfind("/") + 1]
      self.outputImagesDirectory.currentPath = outputPath
      self.runButton.enabled = True


  def getParams(self):
    # Append mandatory options
    params = {}
    params["-i"] = self.inputPath
    params["-o"] = self.outputImagesDirectory.currentPath + self.outputImagesDefault
    params["-v"] = self.verbosityDefault

    # Append general options
    if str(self.addSelector.value) != "0":
      params["-add"] = str(self.addSelector.value)
    if self.createSelector.getLabels():
      params["-create"] = self.createSelector.getLabelsString()
    if self.createAddSelector.getLabels():
      params["-create-add"] = self.createAddSelector.getLabelsString()
    if self.createSegSelector.getLabels():
      params["-create-seg"] = self.createSegSelector.getLabelsString()
    if int(str(self.crossSelector.value)) != self.crossDefault:
      params["-cross"] = str(self.crossSelector.value)
    if self.cubicToPointSelector.checked:
      params["-cubic-to-point"] = ""
    if self.displaySelector.checked: params["-display"] = ""
    if self.incrementSelector.checked:
      params["-increment"] = ""
    if str(self.vertbodySelector.text):
      params["-vert-body"] = str(self.vertbodySelector.text)
    if str(self.vertcontinuousSelector.text):
      params["-vert-continuous"] = str(self.vertcontinuousSelector.text)
    if self.MSEImageSelector.currentNode():
      params["-MSE"] = self.MSEImageSelector.currentNode().GetStorageNode().GetFileName()
    if self.removeSelector.currentNode():
      params["-remove"] = self.removeSelector.currentNode().GetStorageNode().GetFileName()
    if self.removesymSelector.currentNode():
      params["-remove-sym"] = self.removesymSelector.current().GetStorageNode().GetFileName()

    return params

  def onRunButton(self):
    logic = SCTLabelUtilsLogic()
    params = self.getParams()
    if len(params.keys()) < 4:
      self.statusLabel.text = "Status: must select at least one process"
    else:
      statusCode = logic.run(params)
      self.statusLabel.text = self.statusDict[statusCode]

  def cleanup(self):
    pass

#
# SCTLabelUtilsLogic
#

class SCTLabelUtilsLogic(ScriptedLoadableModuleLogic):

  def run(self, params_dict):
    """calls sct_utils.run to run command
    Adds sct_scripts to path by looking at bash variables
    Returns status code:
    statusCode == 0  : sct_label_utils ran without any error
    statusCode == 1  : sct_label_utils ran successfully but could not load output
    statusCode == -1 : sct_label_utils did not run because sct/scripts dir was not found
    statusCode == -2 : sct_label_utils ran and threw some exception
    """


    statusCode = 0

    try:
      if self.addSCTScriptsToPath():

        import sct_utils as sct
        import sct_label_utils
        from msct_parser import Parser

        params_str = Parser.dictionary_to_string(sct_label_utils.get_parser(), params_dict)

        sct.run("sct_label_utils" + params_str)

        # load output volumes

        self.removeNodeIfExists("labels")

        loadLabels, labels = slicer.util.loadVolume(params_dict["-o"], {"show" : False}, True)
        if not loadLabels: statusCode = 1
        else: labels.SetName("labels")

      else:
        statusCode = -1

    except:  # Prevent Slicer from crashing
      statusCode = -2
      print(sys.exc_info())

    return statusCode

  def removeNodeIfExists(self, nodeName):
    node = slicer.util.getNode(nodeName)
    if node:
      slicer.mrmlScene.RemoveNode(node)


  def addSCTScriptsToPath(self):
    '''Finds sct/bin directory in system path and adds sct/scripts to system path variable
    returns True if sct/bin was found in system path, else returns false'''

    paths = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          sct_scripts_path =  sct_bin_path[:sct_bin_path.rfind("/")] + "/scripts"
          sys.path.append(sct_scripts_path)
          return True
      return False
    except KeyError:  # path variable not found
      return False

class SCTLabelUtilsTest(ScriptedLoadableModuleTest):

  def setUp(self):
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_SCTLabelUtils()

  def test_SCTLabelUtils(self):
    """Calls sct_testing to carry out the testing
    """
    self.delayDisplay('No tests available')
    # TODO


class LabelsCreationFormWidget(qt.QVBoxLayout):

  def __init__(self, title="", axes="xyz"):
    '''Create UI for label for the axes provided'''
    qt.QVBoxLayout.__init__(self)
    groupBox = ctk.ctkCollapsibleGroupBox()
    groupBox.setTitle(title)
    self.addWidget(groupBox)
    self.form_layout = qt.QFormLayout()
    groupBox.setLayout(self.form_layout)
    self.setContentsMargins(0, 5, 0, 5)  # vertical margins make the titles easier to read

    self.axes = axes

    self.numLabelsSelector = qt.QSpinBox()
    self.form_layout.addRow("Number of labels", self.numLabelsSelector)

    self.labelCreationWidgets = []

    self.numLabelsSelector.connect('valueChanged(int)', self.changeNumLabelCreationWidget)

  def changeNumLabelCreationWidget(self):
    numLabels = self.numLabelsSelector.value
    if numLabels > len(self.labelCreationWidgets):
      for i in range(len(self.labelCreationWidgets), numLabels):
        labelCreationWidget = LabelCreationWidget(self.axes)
        self.labelCreationWidgets.append(labelCreationWidget)
        self.form_layout.addRow(labelCreationWidget)
    else:
      for i in range(numLabels, len(self.labelCreationWidgets)):
        labelCreationWidget = self.labelCreationWidgets.pop()
        labelCreationWidget.delete()

  def getLabels(self):
    """gets a list of labels from all containing label creation widgets
    """
    labels = []
    for labelCreationWidget in self.labelCreationWidgets:
      label = labelCreationWidget.getLabel()
      if label:
        labels.append(label)
    return labels

  def getLabelsString(self, delim=":"):
    """returns a string of labels with comma separated coords and values
    with each label separated by delim
    """
    labels = self.getLabels()
    labels_str = ""
    for label in labels:
      labels_str += ",".join(label)
      labels_str += delim

    # remove final delim
    labels_str = labels_str[:-1]

    return labels_str

class LabelCreationWidget(qt.QGridLayout):

  def __init__(self, axes="xyz"):
    """Create a widget that prompts user for label value and coordinates
    string axes argument is used in case where different coordinate systems are desirable
    """
    qt.QGridLayout.__init__(self)

    self.labelValLabel = qt.QLabel("Label value")
    self.labelValSelector = qt.QSpinBox()

    formLayout = qt.QFormLayout()
    formLayout.addRow(self.labelValLabel, self.labelValSelector)

    self.addLayout(formLayout, 1,1)

    self.axes = axes
    self.labelSelectorDict = {}
    column = 2

    for axis in self.axes:
      self.labelSelectorDict[axis] = [qt.QLabel(axis + ":"), qt.QSpinBox()]
      formLayout = qt.QFormLayout()
      formLayout.addRow(self.labelSelectorDict[axis][0], self.labelSelectorDict[axis][1])
      self.addLayout(formLayout, 1, column)
      column += 2

  def getLabel(self):
    """returns a list with coordinates with label value being index 0
    """
    label = []
    if self.labelValSelector.text == "":
      return None  # return empty label when label value is empty
    else:
      label.append(str(self.labelValSelector.value))

    for axis in self.axes:
      label.append(str(self.labelSelectorDict[axis][1].value))

    return label

  def delete(self):
    self.labelValLabel.deleteLater()
    self.labelValSelector.deleteLater()

    for axis in self.axes:
      self.labelSelectorDict[axis][0].deleteLater()
      self.labelSelectorDict[axis][1].deleteLater()
      self.deleteLater()
