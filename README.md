# SCT Wrappers README #

The recommended way to use the SCT Wrappers modules is through the Docker image: vsivan/sct_slicer

## Setting up the Docker image ##

#### Pull the Docker image ####

Open a terminal and type the following command to pull the Docker image onto your machine.

```
sudo docker pull vsivan/sct_slicer:latest
```

#### Download and install NVIDIA driver ####

To check what version of NVIDIA is running on the host, run the command:

```
nvidia-smi
```

Download an installer for the same version of NVIDIA from [here](http://www.nvidia.com/Download/index.aspx?lang=en-us) for your specific opersating system.

To install the driver in the docker image, first run the docker image with the command:

```
sudo docker run -it -volume=/path/to/NVIDIADRIVER/:/tmp/NVIDIA vsivan/sct_slicer
```

Inside the Docker container navigate to the directory with the NVIDIA driver:

`cd /tmp/NVIDIA`

and run the following commands in the container:

```
sh /tmp/NVIDIA-DRIVER.run -a -N --ui=none -no-kernel-module
rm /tmp/NVIDIA-DRIVER.run
```

where NVIDIA-DRIVER.run is replaced by the name of the NVIDIA installer.

#### Update SCT Wrappers ####

The version of the SCT Wrappers repo might not be up-to-date. In order to ensure that the
latest version is being used, run the following commands in the container running the Docker image:

```
cd /usr/local/git/SCTWrappers/  # navigate to repo dir
git checkout master
git pull origin master
```

## Building the Docker image from the Dockerfiles ##

The Dockerfiles have to be built in the following order,


  1. compiled_slicer : Slicer is built with custom parameters and a specific version of ITK

  2. installed_slicer : ANTs is built with Slicer's ITK and VTK

  3. sct_slicer : SCT is installed and binaries from ANTs are copied to bin directory

  4. sct\_slicer\_configured : SCT binaries are built, isct\_propseg and isct\_orientation3D are built again and copied over to bin. Finally, the NVIDIA driver is installed.

Note: it is necessary to modify the ```FROM``` command in the Dockerfile if modifications to the Dockerfile are made to build a custom image.

## Setting up an environment independent of Docker##

The Dockerfiles can be used as a guide for setting up Slicer, SCT and ANTs for the running of the SCT Wrappers.

- For the building of Slicer, refer to the Dockerfile for compiled\_slicer.

- For the building of ANTs using Slicer's ITK and VTK, refere to the Dockerfile for installed\_slicer.

- Finally, refer to the Dockerfile for sct\_slicer\_configured for information about how to build binaries and which binaries need to be rebuilt. 

#### Installing wrappers and set up Slicer module paths ####

Clone the sctwrappers repo from Bitbucket onto your local machine.

```
cd <directory to download git repository>
git clone git@bitbucket.org:OrthopaedicBiomechanicsLab/sctwrappers.git
```

Clone the tractography repo from Bitbucket onto your local machine.

```
cd <directory to download git repository>
git clone git@bitbucket.org:OrthopaedicBiomechanicsLab/tractography.git

```

Change the slicer settings to add the module paths.

  1. Open Slicer and go to Edit > Application Settings
  2. Go to the Modules Tab and add additional module paths for each module. Press OK to save.
  
## Running the SCT wrappers ##

Note: many of the commands take some time to run and may freeze the GUI.

Information about the parameters for the SCT Wrappers can be found here:
https://sourceforge.net/p/spinalcordtoolbox/wiki/Home/

#### SCT Propseg ####

1. Select the input volume from the drop-down box.
2. Select the desired contrast from the drop-down box. If the input volume contains the contrast name (e.g. t1.nii.gz) the corresponding contrast (t1) will be automatically selected.
3. Set the segmentation parameters. More information about the segmentation parameters can be found on the SCT wiki.
4. Click Run SCT Propseg button

#### SCT Label Vertebrae ####

1. Select the input volume from the drop-down box.
2. Select the segmented volume from the drop-down box. If the segmented image was obtained using SCT and the segmented image was loaded correctly, it will be selected automatically.
3. Select the desired contrast from the drop-down box. If the input volume contains the contrast name (e.g. t1.nii.gz) the corresponding contrast (t1) will be automatically selected.
4. Select the various parameters for the labelling. More information about the parameters can be found on the SCT wiki.
5. Click Run SCT Label Vertebrae

#### SCT Label Utils ####

1. Select the labelled volume from the drop-down box.
2. Select a process to carry out. For the SCT Tractography pipeline, a labels file needs to be created for vertebral levels. To select vertebral levels, type the comma-separated levels into the text box (e.g. 2,6 for levels 2 to 6). More information about the other process can be found in the SCT wiki.
3. Click Run SCT Label Vertebrae

#### SCT Register To Template ####

1. Select the anatomical image and segmented image from the drop-down boxes.
2. Select the labels file from the drop-down box
3. The parameters for registration can be modified using the format specified in the SCT wiki.
4. Click Run SCT Register To Template button

#### SCT Warp Template ####

1. Select the destination image and the warping fields from the drop-down boxes
2. Click Run SCT Warp Template

#### SCT DMRI Separate B0 DWI ####

1. Select the diffusion data from the drop down menu
2. Select the paths to the bvecs file and the bvals files from the menu. If the bvecs and bvals file exist in the same directory as the diffusion data, these will be selected automatically
3. Click Run SCT DMRI Separate B0 DWI

#### SCT Register Multimodal ####

1. Select the image source volume and the image destination volumes from the drop down menu
2. Select the various general options from the menu. More information about the general options can be found in the SCT wiki. Note that when running this module with the parameters used in the SCT pipeline, it is necessary to load the PAM50 template files into Slicer in order to select them.
3. The parameters for registration can be set for each step of the registration. The parameters can be specified either by entering the raw parameters in the text box as defined in the SCT wiki or through the GUI. To specify parameters for a step, click on Add Step. Modify the parameters as needed and click on Set parameters to add the parameters for that step.
The parameters will be shown on the textbox. Clicking on close does not add the parameters to the parameter list and will not add the parameters to the text box. Parameter steps can be removed by clicking Remove Last Step. To facilitate the proper selection of parameters, some values cannot be modified unless other values are selected. More information about these parameters can also be found on the GUI.
4. Click Run SCT Register Multimodal

#### SCT Concat Transfo ####

1. Select the destination image from the drop-down menu
2. Modify the number of images that will be concatenated. Changing the number of images changes the number of fields available to add images accordingly.
3. Select the warp images from the drop-down lists
4. Select the output warp field directory using the directory selector.

## Running the SCT Tractography Pipeline ##

#### SCT Tractography Pipeline ####

1. Select the weighted image and the dmri image from the drop-down lists provided.
2. The Run Tractography Pipeline option can be unchecked if the running of the tractography pipeline is not desired.
3. Parameters for each step of the pipeline can be modified by clicking on Options. This opens a pop-up window with a list of each step and the user interface for each step.
4. For all of the SCT steps, the UI is exactly the same as that for the corresponding SCT wrapper. The final step has a custom UI that defines fields for most of the parameters that will be used in the json file that will be generated for the tractography pipeline. To save modifications, either close the window or click the Apply button.
5. Click Run SCT Tractography Pipeline

## Screenshots: ##

Screenshots of the modules with valid input parameters can be found [here](Screenshots).
