import sys
import os
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import SimpleITK as sitk
import sitkUtils

#
# SCTConcatTransfo
#

class SCTConcatTransfo(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
 """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "SCT Concat Transfo"
    self.parent.categories = ["Spinal Cord Toolbox"]
    self.parent.dependencies = []
    self.parent.contributors = ["Vignesh Sivan (Sunnybrook)"]
    self.parent.helpText = """
Concatenate transformations. This function is a wrapper for sct_ComposeMultiTransform (ANTs).
N.B. Order of input warping fields is important. For example, if you want to concatenate: A->B and
B->C to yield A->C, then you have to input warping fields like that: A->B,B->C
<p> For more information visit the <a href="https://sourceforge.net/p/spinalcordtoolbox/wiki/sct_concat_transfo/"> spinalcordtoolbox documentation </a> </p>"""

    self.parent.helpText += ""
    self.parent.acknowledgementText = ""

#
# SCTConcatTransfoWidget
#

class SCTConcatTransfoWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):

    ScriptedLoadableModuleWidget.setup(self)

    self.statusDict = {}
    self.statusDict[0] = "Status: SCT Concat Transfo ran successfully"
    self.statusDict[1] = "Status: Could not load output files from SCT Concat Transfo"
    self.statusDict[-1] = "Status: SCT Concat Transfo did not run because sct/scripts directory was not found"
    self.statusDict[-2] = "Status: SCT Concat Transfo threw some exception"

    #
    # Main Options
    #

    mandatoryOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    mandatoryOptionsCollapsibleButton.text = "Mandatory Options"
    self.layout.addWidget(mandatoryOptionsCollapsibleButton)

    mandatoryOptionsLayout = qt.QFormLayout(mandatoryOptionsCollapsibleButton)

    self.destinationImageSelector = slicer.qMRMLNodeComboBox()
    self.destinationImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.destinationImageSelector.selectNodeUponCreation = True
    self.destinationImageSelector.addEnabled = False
    self.destinationImageSelector.removeEnabled = True
    self.destinationImageSelector.noneEnabled = True
    self.destinationImageSelector.showHidden = False
    self.destinationImageSelector.showChildNodeTypes = False
    self.destinationImageSelector.setMRMLScene( slicer.mrmlScene )

    mandatoryOptionsLayout.addRow("Destination image", self.destinationImageSelector)

    # number of warp images

    self.numberOfImagesSelector = qt.QSpinBox()
    self.numberOfImagesSelector.setValue(2)
    mandatoryOptionsLayout.addRow("Number of images", self.numberOfImagesSelector)

    self.warpImagesLayout = qt.QFormLayout()
    self.warpImageSelectors = []

    warpImagesGroupBox = qt.QGroupBox()
    warpImagesGroupBox.setLayout(self.warpImagesLayout)

    mandatoryOptionsLayout.addRow(warpImagesGroupBox)

    self.changeNumImages()

    #
    # General Options
    #

    generalOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    generalOptionsCollapsibleButton.text = "General Options"
    self.layout.addWidget(generalOptionsCollapsibleButton)

    generalOptionsLayout = qt.QFormLayout(generalOptionsCollapsibleButton)

    self.outputWarpFieldSelector = ctk.ctkPathLineEdit()
    generalOptionsLayout.addRow("Output warp field", self.outputWarpFieldSelector)

    #
    # Run button
    #

    self.runButton = qt.QPushButton("Run SCT Concat Transfo")
    self.runButton.enabled = False

    self.layout.addWidget(self.runButton)

    # Status label
    self.statusLabel = qt.QLabel("Status: ")
    self.layout.addWidget(self.statusLabel)

    #
    # Connections
    #

    self.destinationImageSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelectDestinationImage)
    self.runButton.connect('clicked(bool)', self.onRunButton)
    self.numberOfImagesSelector.connect('valueChanged(int)', self.changeNumImages)

  def changeNumImages(self):
    numImages = self.numberOfImagesSelector.value
    if numImages > len(self.warpImageSelectors):
      for i in range(len(self.warpImageSelectors), numImages):

        warpImageSelector = slicer.qMRMLNodeComboBox()
        warpImageSelector.nodeTypes = ["vtkMRMLVectorVolumeNode"]
        warpImageSelector.selectNodeUponCreation = True
        warpImageSelector.addEnabled = False
        warpImageSelector.removeEnabled = True
        warpImageSelector.noneEnabled = True
        warpImageSelector.showHidden = False
        warpImageSelector.showChildNodeTypes = False
        warpImageSelector.setMRMLScene( slicer.mrmlScene )

        self.warpImageSelectors.append(warpImageSelector)

        self.warpImagesLayout.addRow("Warp image %s :" % str(i + 1), warpImageSelector)
    else:
      for i in range(numImages, len(self.warpImageSelectors)):
        warpImageSelector = self.warpImageSelectors.pop()
        warpImageLabel = self.warpImagesLayout.labelForField(warpImageSelector)
        warpImageLabel.deleteLater()
        warpImageSelector.deleteLater()


  def onSelectDestinationImage(self):

    destinationImage = self.destinationImageSelector.currentNode()

    if destinationImage:

      self.runButton.enabled = True
      destinationImagePath = destinationImage.GetStorageNode().GetFileName()
      destinationImageDir = destinationImagePath[:destinationImagePath.rfind("/") + 1]


  def getParams(self):

    warpImagePaths = ""

    # first check if there are warp images
    for warpImageSelector in self.warpImageSelectors:
        if warpImageSelector.currentNode():
            warpImagePaths += warpImageSelector.currentNode().GetStorageNode().GetFileName() + ","

    # remove the final comma
    if warpImagePaths[-1] == ",":
        warpImagePaths = warpImagePaths[:-1]

    params = {}
    params["-d"] = self.destinationImageSelector.currentNode().GetStorageNode().GetFileName()
    params["-w"] = warpImagePaths
    params["-o"] = self.outputWarpFieldSelector.currentPath

    return params

  def onRunButton(self):
    logic = SCTConcatTransfoLogic()

    params = self.getParams()

    if len(self.warpImageSelectors) == 0 or params["-w"] == "":
        self.statusLabel.text = "Status: No warp image selected"

    else: statusCode = logic.run(params)
    self.statusLabel.text = self.statusDict[statusCode]

  def cleanup(self):
    pass

#
# SCTConcatTransfoLogic
#

class SCTConcatTransfoLogic(ScriptedLoadableModuleLogic):

  def run(self, params_dict):
    """Calls sct_utils.run to run sct_concat_transfo
    Adds sct_scripts to path by looking at bash variables
    Returns status code:
    statusCode == 0  : sct_concat_transfo ran without any error
    statusCode == 1  : sct_concat_transfo ran successfully but could not load output
    statusCode == -1 : sct_concat_transfo did not run because sct/scripts dir was not found
    statusCode == -2 : sct_concat_transfo ran and threw some exception
    """

    statusCode = 0

    try:

      if self.add_sct_scripts_to_path():

        import sct_utils as sct
        import sct_concat_transfo
        from msct_parser import Parser

        params_str = Parser.dictionary_to_string(sct_concat_transfo.get_parser(), params_dict)

        sct.run("sct_concat_transfo " + params_str)

        # load output data
        outputPath = os.getcwd() + "/"

        if params_dict["-o"]:
          outputPath = params_dict["-o"]

        outputName = outputPath[outputPath.rfind("/") + 1:outputPath.find(".")]

        self.removeNodeIfExists(outputName)

        load_output, outputWarp = slicer.util.loadVolume(outputPath, {"show" : False}, True)

        if not load_output: statusCode = 1
        else: outputWarp.SetName(outputName)

      else:
        statusCode = -1

    except: # Prevent Slicer from crashing
      statusCode = -2
      print(sys.exc_info())

    return statusCode

  def removeNodeIfExists(self, nodeName):
    node = slicer.util.getNode(nodeName)
    if node:
      slicer.mrmlScene.RemoveNode(node)

  def add_sct_scripts_to_path(self):
    '''Finds sct/bin directory in system path and adds sct/scripts to system path variable
    returns True if sct/bin was found in system path, else returns false'''

    paths = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          sct_scripts_path =  sct_bin_path[:sct_bin_path.rfind("/")] + "/scripts"
          sys.path.append(sct_scripts_path)
          return True
      return False
    except KeyError:  # path variable not found
      return False

class SCTConcatTransfoTest(ScriptedLoadableModuleTest):

  def setUp(self):
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_SCTConcatTransfo()

  def test_SCTConcatTransfo(self):
    """Calls sct_testing to carry out the testing
    """
    self.delayDisplay('No tests available')
