import sys
import os
from multiprocessing.pool import ThreadPool
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import SimpleITK as sitk
import sitkUtils

#
# SCTPropseg
#

class SCTPropseg(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "SCT Propseg"
    self.parent.categories = ["Spinal Cord Toolbox"]
    self.parent.contributors = ["Vignesh Sivan (Sunnybrook)"]
    self.parent.helpText = """
<p>This program segments automatically the spinal cord on INPUT- and T2-weighted images, for any field of view. You must provide the type of contrast, the image as well as the output folder path.</p>

<p>Initialization is provided by a spinal cord detection module based on the elliptical Hough transform on multiple axial slices. The result of the detection is available as a PNG image using option -detection-png.</p>

Parameters of the spinal cord detection are :
<ul>
    <li> the position (in inferior-superior direction) of the initialization </li>
    <li> the number of axial slices </li>
    <li> the gap (in pixel) between two axial slices </li>
    <li> the approximate radius of the spinal cord </li>
</ul>

<p>Primary output is the binary mask of the spinal cord segmentation. This method must provide VTK triangular mesh of the segmentation (option -mesh). Spinal cord centerline is available as a binary image (-centerline-binary) or a text file with coordinates in world referential (centerline coord as * image).</p>

<p>Cross-sectional areas along the spinal cord can be available (by selecting option cross sectional areas).</p>

<p>If the segmentation fails at some location (e.g. due to poor contrast between spinal cord and CSF), edit your anatomical image (e.g. with fslview) and manually enhance the contrast by adding bright values around the spinal cord for T2-weighted images (dark values for INPUT-weighted). Then, launch the segmentation again.</p>

<p> For more information visit the <a href="https://sourceforge.net/p/spinalcordtoolbox/wiki/sct_propseg/"> spinalcordtoolbox documentation </a> </p>
    """
    self.parent.acknowledgementText = ""

#
# SCTPropsegWidget
#

class SCTPropsegWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # defaults

    self.downlimitDefault       = 0
    self.upLimitMax             = self.downlimitDefault
    self.radiusDefault          = 4
    self.detectnDefault         = 4
    self.nbiterMax              = 1000
    self.nbiterDefault          = 200
    self.detectgapDefault       = 4
    self.maxareaDefault         = 120
    self.maxareaMax             = 500
    self.maxdeformationDefault  = 2.5
    self.mincontrastDefault     = 50
    self.tradeoffMin            = 0
    self.tradeoffMax            = 50
    self.distancesearchmaximum  = 30
    self.distancesearchminimum  = 15
    self.distancesearchDefault  = 15
    self.alphaMax               = 50
    self.alphaMin               = 0
    self.alphaDefault           = 25
    self.removeTempFilesDefault = 1
    self.displayDefault         = 0  # cannot be modified through the UI

    self.statusDict = {}
    self.statusDict[0] = "Status: SCT Propseg ran successfully"
    self.statusDict[1] = "Status: Could not load output files from SCT Propseg"
    self.statusDict[-1] = "Status: SCT Propseg did not run because sct directory was not found"
    self.statusDict[-2] = "Status: SCT Propseg threw some exception"

    #
    # Main Options
    #

    mandatoryOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    mandatoryOptionsCollapsibleButton.text = "Mandatory Options"
    self.layout.addWidget(mandatoryOptionsCollapsibleButton)

    mandatoryOptionsLayout = qt.QFormLayout(mandatoryOptionsCollapsibleButton)

    self.inputImageSelector = slicer.qMRMLNodeComboBox()
    self.inputImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.inputImageSelector.selectNodeUponCreation = True
    self.inputImageSelector.addEnabled = False
    self.inputImageSelector.removeEnabled = True
    self.inputImageSelector.noneEnabled = True
    self.inputImageSelector.showHidden = False
    self.inputImageSelector.showChildNodeTypes = False
    self.inputImageSelector.setMRMLScene( slicer.mrmlScene )

    mandatoryOptionsLayout.addRow("Input image", self.inputImageSelector)

    self.contrastSelector = qt.QComboBox()
    self.contrastSelector.addItems(['t1','t2','t2s','dwi'])
    mandatoryOptionsLayout.addRow("Contrast", self.contrastSelector)

    #
    # General Options
    #

    generalOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    generalOptionsCollapsibleButton.text = "General Options"
    self.layout.addWidget(generalOptionsCollapsibleButton)

    generalOptionsLayout = qt.QFormLayout(generalOptionsCollapsibleButton)

    self.outputFolderSelector = ctk.ctkPathLineEdit()
    self.outputFolderSelector.filters = ctk.ctkPathLineEdit.Dirs
    generalOptionsLayout.addRow("Output folder", self.outputFolderSelector)

    self.downlimitSelector = ctk.ctkSliderWidget()
    self.downlimitSelector.minimum = 0
    self.downlimitSelector.maximum = self.upLimitMax
    self.downlimitSelector.value = self.downlimitDefault

    generalOptionsLayout.addRow("Down limit of propagation", self.downlimitSelector)

    self.upLimitSelector = ctk.ctkSliderWidget()
    self.upLimitSelector.minimum = 0
    self.upLimitSelector.maximum = self.upLimitMax
    self.upLimitSelector.value = self.downlimitDefault

    generalOptionsLayout.addRow("Up limit of propagation", self.upLimitSelector)

    self.removeTempFilesSelector = qt.QCheckBox()
    self.removeTempFilesSelector.setChecked(self.removeTempFilesDefault)

    generalOptionsLayout.addRow("Remove temporary files", self.removeTempFilesSelector)

    #
    # Ouput options
    #

    outputOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    outputOptionsCollapsibleButton.text = "Output Options"
    self.layout.addWidget(outputOptionsCollapsibleButton)

    outputOptionsLayout = qt.QFormLayout(outputOptionsCollapsibleButton)

    self.meshSelector = qt.QCheckBox()
    self.meshSelector.checked = 0
    outputOptionsLayout.addRow("Mesh of spinal cord segmentation", self.meshSelector)

    self.centerlinebinarySelector= qt.QCheckBox()
    self.centerlinebinarySelector.checked = 0
    outputOptionsLayout.addRow("Centerline as a binary image", self.centerlinebinarySelector)

    self.CSFSelector= qt.QCheckBox()
    self.CSFSelector.checked = 0
    outputOptionsLayout.addRow("CSF segmentation", self.CSFSelector)

    self.centerlinecoordSelector= qt.QCheckBox()
    self.centerlinecoordSelector.checked = 0
    outputOptionsLayout.addRow("Centerline in world coordinates", self.centerlinecoordSelector)

    self.crossSelector= qt.QCheckBox()
    self.crossSelector.checked = 0
    outputOptionsLayout.addRow("Cross sectional areas", self.crossSelector)

    self.inittubeSelector= qt.QCheckBox()
    self.inittubeSelector.checked = 0
    outputOptionsLayout.addRow("Initial tubular meshes", self.inittubeSelector)

    self.lowresolutionmeshSelector= qt.QCheckBox()
    self.lowresolutionmeshSelector.checked = 0
    outputOptionsLayout.addRow("Low resolution mesh", self.lowresolutionmeshSelector)

    self.detectniiSelector= qt.QCheckBox()
    self.detectniiSelector.checked = 0
    outputOptionsLayout.addRow("Spinal cord detection as a NIFTI image", self.detectniiSelector)

    self.detectpngSelector= qt.QCheckBox()
    self.detectpngSelector.checked = 0
    outputOptionsLayout.addRow("Spinal cord detection as a PNG image", self.detectpngSelector)

    #
    # Initialization - Spinal Cord Detection Options
    #

    initOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    initOptionsCollapsibleButton.text = "Spinal cord detection options"
    self.layout.addWidget(initOptionsCollapsibleButton)

    initOptionsLayout = qt.QFormLayout(initOptionsCollapsibleButton)

    self.initmaskSelector = slicer.qMRMLNodeComboBox()
    self.initmaskSelector.nodeTypes = ["vtkMRMLScalarVolumeNode", "vtkMRMLLabelMapVolumeNode"]
    self.initmaskSelector.selectNodeUponCreation = True
    self.initmaskSelector.addEnabled = False
    self.initmaskSelector.removeEnabled = True
    self.initmaskSelector.noneEnabled = True
    self.initmaskSelector.showHidden = False
    self.initmaskSelector.showChildNodeTypes = False
    self.initmaskSelector.setMRMLScene( slicer.mrmlScene )

    initOptionsLayout.addRow("Mask containing the center of the spinal cord", self.initmaskSelector)

    self.maskcorrectionSelector = slicer.qMRMLNodeComboBox()
    self.maskcorrectionSelector.nodeTypes = ["vtkMRMLScalarVolumeNode", "vtkMRMLLabelMapVolumeNode"]
    self.maskcorrectionSelector.selectNodeUponCreation = True
    self.maskcorrectionSelector.addEnabled = False
    self.maskcorrectionSelector.removeEnabled = True
    self.maskcorrectionSelector.noneEnabled = True
    self.maskcorrectionSelector.showHidden = False
    self.maskcorrectionSelector.showChildNodeTypes = False
    self.maskcorrectionSelector.setMRMLScene( slicer.mrmlScene )

    initOptionsLayout.addRow("Mask correction", self.maskcorrectionSelector)

    self.radiusSelector =  ctk.ctkDoubleSpinBox()
    self.radiusSelector.setValue(self.radiusDefault)
    initOptionsLayout.addRow("Approx. radius of the spinal cord (mm)", self.radiusSelector)

    self.initSelector =  ctk.ctkDoubleSpinBox()
    initOptionsLayout.addRow("Axial slice where the propagation starts", self.initSelector)

    self.detectnSelector =  ctk.ctkDoubleSpinBox()
    self.detectnSelector.setValue(self.detectnDefault)
    initOptionsLayout.addRow("Number of axial slices computed in detection process",
                             self.detectnSelector)

    self.detectgapSelector =  ctk.ctkDoubleSpinBox()
    self.detectgapSelector.setValue(self.detectgapDefault)
    initOptionsLayout.addRow("Gap in z direction for the detection process", self.detectgapSelector)

    self.initvalidationSelector =  ctk.ctkDoubleSpinBox()
    initOptionsLayout.addRow("Enable validation on spinal cord detection",
                             self.initvalidationSelector)

    #
    # Help segmentation section
    #

    helpsegCollapsibleButton = ctk.ctkCollapsibleButton()
    helpsegCollapsibleButton.text = "Propagation options"
    self.layout.addWidget(helpsegCollapsibleButton)

    helpsegOptionsLayout = qt.QFormLayout(helpsegCollapsibleButton)

    self.initcenterlineSelector = slicer.qMRMLNodeComboBox()
    self.initcenterlineSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.initcenterlineSelector.selectNodeUponCreation = True
    self.initcenterlineSelector.addEnabled = False
    self.initcenterlineSelector.removeEnabled = True
    self.initcenterlineSelector.noneEnabled = True
    self.initcenterlineSelector.showHidden = False
    self.initcenterlineSelector.showChildNodeTypes = False
    self.initcenterlineSelector.setMRMLScene( slicer.mrmlScene )

    helpsegOptionsLayout.addRow("Centerline for propagation", self.initcenterlineSelector)

    self.nbiterSelector =  qt.QSpinBox()
    self.nbiterSelector.setMaximum(self.nbiterMax)
    self.nbiterSelector.setValue(self.nbiterDefault)
    helpsegOptionsLayout.addRow("Number of iterations", self.nbiterSelector)

    self.maxareaSelector =  qt.QDoubleSpinBox()
    self.maxareaSelector.setMaximum(self.maxareaMax)
    self.maxareaSelector.setValue(self.maxareaDefault)
    helpsegOptionsLayout.addRow("Maximum area (mm^2)", self.maxareaSelector)

    self.maxdeformationSelector =  ctk.ctkDoubleSpinBox()
    self.maxdeformationSelector.setValue(self.maxdeformationDefault)
    helpsegOptionsLayout.addRow("Maximum deformation (mm)", self.maxdeformationSelector)

    self.mincontrastSelector =  ctk.ctkDoubleSpinBox()
    self.mincontrastSelector.setValue(self.mincontrastDefault)
    helpsegOptionsLayout.addRow("Min contrast", self.mincontrastSelector)

    self.tradeoffSelector =  ctk.ctkSliderWidget()
    self.tradeoffSelector.minimum = self.tradeoffMin
    self.tradeoffSelector.maximum = self.tradeoffMax
    helpsegOptionsLayout.addRow("Trade off", self.tradeoffSelector)

    self.distancesearchSelector = ctk.ctkSliderWidget()
    self.distancesearchSelector.minimum = self.distancesearchminimum
    self.distancesearchSelector.maximum = self.distancesearchmaximum
    self.distancesearchSelector.value = self.distancesearchDefault
    helpsegOptionsLayout.addRow("Distance search", self.distancesearchSelector)

    self.alphaSelector =  ctk.ctkSliderWidget()
    self.alphaSelector.maximum = self.alphaMax
    self.alphaSelector.minimum = self.alphaMin
    self.alphaSelector.value = self.alphaDefault
    helpsegOptionsLayout.addRow("Alpha", self.alphaSelector)

    # quality control

    self.noqcSelector = qt.QGroupBox("Generate quality control report")
    self.noqcSelector.setCheckable(True)
    self.noqcSelector.setChecked(False)

    self.qcSelector = ctk.ctkPathLineEdit()
    self.qcSelector.filters = ctk.ctkPathLineEdit.Dirs

    qcLayout = qt.QFormLayout()
    qcLayout.addRow("Path to QC report", self.qcSelector)

    self.noqcSelector.setLayout(qcLayout)

    helpsegOptionsLayout.addRow(self.noqcSelector)

    # Run button

    self.runButton = qt.QPushButton("Run SCT Propseg")
    self.runButton.enabled = False

    self.layout.addWidget(self.runButton)

    # Status

    self.statusLabel = qt.QLabel("Status: ")
    self.layout.addWidget(self.statusLabel)

    #
    # Connections
    #

    self.inputImageSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelectInputImage)
    self.runButton.connect('clicked(bool)', self.onRunButton)

    self.onSelectInputImage()

  def onSelectInputImage(self):

    inputNode = self.inputImageSelector.currentNode()

    if inputNode:
      self.runButton.enabled = True

      inputStorageNode = inputNode.GetStorageNode()
      self.path = inputStorageNode.GetFileName()

      # set contrast based on node name
      contrasts = [self.contrastSelector.itemText(i) for i in range(self.contrastSelector.count)]
      for contrast in contrasts:
        if contrast in inputNode.GetName():
          idx = self.contrastSelector.findText(contrast)
          self.contrastSelector.setCurrentIndex(idx)

      # set up-limit to be equal to number of slices
      imageData = inputNode.GetImageData()
      dimensions = imageData.GetDimensions()
      numSlices = dimensions[2] - 1  # number of slices is the third dimension

      self.upLimitSelector.setValue(numSlices)
      self.upLimitMax = numSlices
      self.upLimitSelector.maximum = self.upLimitMax
      self.downlimitSelector.maximum = self.upLimitMax
      self.upLimitSelector.value = self.upLimitMax

      # set output folder
      self.outputFolderSelector.currentPath = self.path[:self.path.rfind("/")]

      # visualize the input image
      selectionNode = slicer.app.applicationLogic().GetSelectionNode()
      selectionNode.SetReferenceActiveVolumeID(inputNode.GetID())

      # Update view
      applicationLogic = slicer.app.applicationLogic()
      applicationLogic.PropagateVolumeSelection(0)



  def onRunButton(self):
    logic = SCTPropsegLogic()
    params_dict = self.getParams()
    statusCode = logic.run(params_dict)
    self.statusLabel.text = self.statusDict[statusCode]

  def getParams(self):
    # Append mandatory options
    params = {}

    # cannot create configuration file without input image
    if not self.inputImageSelector.currentNode():
      return None

    params["-i"] = self.inputImageSelector.currentNode().GetStorageNode().GetFileName()
    params["-c"] = str(self.contrastSelector.currentText)

    # Append general options
    if self.outputFolderSelector.currentPath:
      params["-ofolder"] = self.outputFolderSelector.currentPath
      # ensure that the output folder ends in a '/'
      if params["-ofolder"][-1] != "/":
        params["-ofolder"] += "/"
    if self.downlimitSelector.value != self.downlimitDefault:
      params["-down"] = str(self.downlimitSelector.value)
    if self.upLimitSelector.value != self.upLimitMax:
      params["-up"] = str(self.upLimitSelector.value)
    if not self.removeTempFilesSelector.isChecked():
      params["-r"] = 0

    # enforce verbosity because the display option is binary and output only shows up
    # in Python interpreter
    params["-v"] =  1

    # Append output options
    if self.meshSelector.isChecked():
      params["-mesh"] = ""
    if self.centerlinebinarySelector.isChecked():
      params["-centerline-binary"] = ""
    if self.CSFSelector.isChecked():
      params["-CSF"] = ""
    if self.centerlinecoordSelector.isChecked():
      params["-centerline-coord"] = ""
    if self.crossSelector.isChecked():
      params["-cross"] = ""
    if self.inittubeSelector.isChecked():
      params["-init-tube"] = ""
    if self.lowresolutionmeshSelector.isChecked():
      params["-low-resolution-mesh"] = ""
    if self.detectniiSelector.isChecked():
      params["-detect-nii"] = ""
    if self.detectpngSelector.isChecked():
      params["-detect-png"] = ""

    # Append options helping the segmentation
    if self.initcenterlineSelector.currentNode():
      params["-init-centerline"] = self.initcenterlineSelector.currentNode().GetStorageNode().\
                                  GetFileName()
    if self.initSelector.value:
      params["-init"] = self.initSelector.value
    if self.initmaskSelector.currentNode():
      params["-init-mask"] = self.initmaskSelector.currentNode().GetStorageNode().GetFileName()
    if self.maskcorrectionSelector.currentNode():
      params["-mask-correction"] = self.maskcorrectionSelector.currentNode().GetStorageNode().\
                                  GetFileName()
    if self.radiusSelector.value != self.radiusDefault:
      params["-radius"] = str(self.radiusSelector.value)
    if self.detectnSelector.value != self.detectnDefault:
      params["-detect-n"] = str(self.detectnSelector.value)
    if self.detectgapSelector.value != self.detectgapDefault:
      params["-detect-gap"] = str(self.detectgapSelector.value)
    if self.initvalidationSelector.value:
      params["-init-value"] = str(self.initvalidationSelector.value)
    if self.nbiterSelector.value != self.nbiterDefault:
      params["-nbiter"] = str(self.nbiterSelector.value)
    if self.maxareaSelector.value != self.maxareaDefault:
      params["-max-area"] = str(self.maxareaSelector.value)
    if self.maxdeformationSelector.value != self.maxdeformationDefault:
      params["-max-deformation"] = str(self.maxdeformationSelector.value)
    if self.mincontrastSelector.value != self.mincontrastDefault:
      params["-min-contrast"] = str(self.mincontrastSelector.value)
    if self.tradeoffSelector.value:
      params["-d"] = str(self.tradeoffSelector.value)
    if self.distancesearchSelector.value != self.distancesearchDefault:
      params["-distance-search"] = str(self.distancesearchSelector.value)
    if self.alphaSelector.value != self.alphaDefault:
      params["-alpha"] = str(self.alphaSelector.value)
    if not self.noqcSelector.isChecked():
      if self.qcSelector.currentPath:
        params[" -qc "] = self.qcSelector.currentPath

    return params


  def cleanup(self):
    pass

#
# SCTPropsegLogic
#

class SCTPropsegLogic(ScriptedLoadableModuleLogic):

  def run(self, params_dict):

    """calls sct_utils.run to run sct_propseg
    Adds sct_scripts to path by looking at bash variables
    Returns status code:
    statusCode == 0  : sct_propseg ran without any error
    statusCode == 1  : sct_propseg ran successfully but could not load output
    statusCode == -1 : sct_propseg did not run because sct/scripts dir was not found
    statusCode == -2 : sct_propseg ran and threw some exception
    """

    statusCode = 0

    try:

      addedSCTScriptsToPath = self.addSCTScriptsToPath()

      if addedSCTScriptsToPath:

        import sct_utils as sct
        import sct_propseg
        from msct_parser import Parser

        params_str = Parser.dictionary_to_string(sct_propseg.get_parser(), params_dict)

        sct.run("sct_propseg " + params_str)

        # load output files from the output directory
        output_dir = ""
        if "-ofolder" in params_dict:
          output_dir = params_dict["-ofolder"]
        else:
          output_dir = os.getcwd()

        ifilepath = params_dict["-i"]
        ifilename = ifilepath[ifilepath.rfind("/") + 1:ifilepath.find(".")]
        ofileprefix = output_dir + ifilename

        # Remove previously loaded volumes with the same name

        segNodeName = ifilename + "_seg"
        centerlineNodeName = ifilename + "_centerline"
        centerlineOpticNodeName = ifilename + "_centerline_optic"

        self.removeNodeIfExists(segNodeName)
        self.removeNodeIfExists(centerlineNodeName)
        self.removeNodeIfExists(centerlineOpticNodeName)

        load_seg, segNode = slicer.util.loadVolume(
            ofileprefix + "_seg.nii.gz",{"labelmap" : True}, True)
        load_centerline, centerlineNode = slicer.util.loadVolume(
            ofileprefix + "_centerline.nii.gz",{"show":False}, True)
        load_centerline_optic, centerlineOpticNode = slicer.util.loadVolume(
            ofileprefix +"_centerline_optic.nii.gz", {"show":False}, True)

        if not load_seg: statusCode = 1
        else:
          segNode.SetName(segNodeName)
          # visualize the output
          selectionNode = slicer.app.applicationLogic().GetSelectionNode()
          selectionNode.SetReferenceActiveLabelVolumeID(segNode.GetID())

        if not load_centerline: statusCode = 1
        else: centerlineNode.SetName(centerlineNodeName)

        if not load_centerline_optic: statusCode = 1
        else: centerlineOpticNode.SetName(centerlineOpticNodeName)

        # update view
        applicationLogic = slicer.app.applicationLogic()
        applicationLogic.PropagateVolumeSelection(0)

      else:
        statusCode = -1

    except:  # Prevent Slicer from crashing
      statusCode = -2
      print(sys.exc_info())

    return statusCode

  def removeNodeIfExists(self, nodeName):
    node = slicer.util.getNode(nodeName)
    if node:
      slicer.mrmlScene.RemoveNode(node)

  def addSCTScriptsToPath(self):
    '''Finds sct/bin directory in system path and adds sct/scripts to system path variable
    returns True if sct/bin was found in system path, else returns false'''

    paths = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          sct_scripts_path =  sct_bin_path[:sct_bin_path.rfind("/") + 1] + "scripts"
          sys.path.append(sct_scripts_path)
          return True
      return True
    except KeyError:  # path variable not found
      return False

class SCTPropsegTest(ScriptedLoadableModuleTest):

  def setUp(self):
    slicer.mrmlScene.Clear(0)
    logic = SCTPropsegLogic

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_SCTPropseg()

  def test_SCTPropseg(self):
    """Calls sct_testing to carry out the testing
    """
    self.addSCTScriptsToPath()
    import sct_testing
    try:
      sct_testing.main(["-f", "sct_propseg"])
      self.delayDisplay('Successfully ran sct_testing')
    except:
      self.delayDisplay("Error running sct_testing")
      print(sys.exc_info())

  def addSCTScriptsToPath(self):
    '''Finds sct/bin directory in system path and adds sct/scripts to system path variable
    returns True if sct/bin was found in system path, else returns false'''

    paths = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          sct_scripts_path =  sct_bin_path[:sct_bin_path.rfind("/") + 1] + "scripts"
          sys.path.append(sct_scripts_path)
          return True
      return True
    except KeyError:  # path variable not found
      return False
