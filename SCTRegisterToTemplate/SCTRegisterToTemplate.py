#TODO: implement a consistent UI for parameters for register to template and register multimodal

import sys
import os
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import SimpleITK as sitk
import sitkUtils

#
# SCTRegisterToTemplate
#

class SCTRegisterToTemplate(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "SCT Register To Template"
    self.parent.categories = ["Spinal Cord Toolbox"]
    self.parent.contributors = ["Vignesh Sivan (Sunnybrook)"]
    self.parent.acknowledgementText = ""
    self.parent.helpText = """
    Register anatomical image to the template.
    <p>For more information, visit the <a href="https://sourceforge.net/p/spinalcordtoolbox/wiki/sct_register_to_template/">spinalcordtoolbox documentation</a></p>
    """

#
# SCTRegisterToTemplateWidget
#

class SCTRegisterToTemplateWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # defaults:
    self.templateFolderDefault = self.findTemplateFolder()

    self.statusDict = {}
    self.statusDict[0] = "Status: SCT Register To Template ran successfully"
    self.statusDict[1] = "Status: Could not load output files from SCT Register To Template"
    self.statusDict[-1] = "Status: SCT Register To Template did not run because sct directory \
    was not found"
    self.statusDict[-2] = "Status: SCT Register To Template threw some exception"

    #
    # Main Options
    #

    mandatoryOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    mandatoryOptionsCollapsibleButton.text = "Mandatory Options"
    self.layout.addWidget(mandatoryOptionsCollapsibleButton)

    mandatoryOptionsLayout = qt.QFormLayout(mandatoryOptionsCollapsibleButton)

    self.anatomicalImageSelector = slicer.qMRMLNodeComboBox()
    self.anatomicalImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.anatomicalImageSelector.selectNodeUponCreation = True
    self.anatomicalImageSelector.addEnabled = False
    self.anatomicalImageSelector.removeEnabled = True
    self.anatomicalImageSelector.noneEnabled = True
    self.anatomicalImageSelector.showHidden = False
    self.anatomicalImageSelector.showChildNodeTypes = False
    self.anatomicalImageSelector.setMRMLScene( slicer.mrmlScene )

    mandatoryOptionsLayout.addRow("Anatomical image", self.anatomicalImageSelector)

    self.segmentedImageSelector = slicer.qMRMLNodeComboBox()
    self.segmentedImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode", "vtkMRMLLabelMapVolumeNode"]
    self.segmentedImageSelector.selectNodeUponCreation = True
    self.segmentedImageSelector.addEnabled = False
    self.segmentedImageSelector.removeEnabled = True
    self.segmentedImageSelector.noneEnabled = True
    self.segmentedImageSelector.showHidden = False
    self.segmentedImageSelector.showChildNodeTypes = False
    self.segmentedImageSelector.setMRMLScene( slicer.mrmlScene )

    mandatoryOptionsLayout.addRow("Segmented image", self.segmentedImageSelector)

    #
    # Optional Arguments
    #

    optionalArgumentsCollapsibleButton = ctk.ctkCollapsibleButton()
    optionalArgumentsCollapsibleButton.text = "Optional Arguments"
    self.layout.addWidget(optionalArgumentsCollapsibleButton)

    optionalArgumentsLayout = qt.QFormLayout(optionalArgumentsCollapsibleButton)

    self.labelsFileSelector = slicer.qMRMLNodeComboBox()
    self.labelsFileSelector.nodeTypes = ["vtkMRMLScalarVolumeNode", "vtkMRMLLabelMapVolumeNode"]
    self.labelsFileSelector.selectNodeUponCreation = True
    self.labelsFileSelector.addEnabled = False
    self.labelsFileSelector.removeEnabled = True
    self.labelsFileSelector.noneEnabled = True
    self.labelsFileSelector.showHidden = False
    self.labelsFileSelector.showChildNodeTypes = False
    self.labelsFileSelector.setMRMLScene( slicer.mrmlScene )

    optionalArgumentsLayout.addRow("Labels File", self.labelsFileSelector)

    self.labelsDiskFileSelector = slicer.qMRMLNodeComboBox()
    self.labelsDiskFileSelector.nodeTypes = ["vtkMRMLScalarVolumeNode", "vtkMRMLLabelMapVolumeNode"]
    self.labelsDiskFileSelector.selectNodeUponCreation = True
    self.labelsDiskFileSelector.addEnabled = False
    self.labelsDiskFileSelector.removeEnabled = True
    self.labelsDiskFileSelector.noneEnabled = True
    self.labelsDiskFileSelector.showHidden = False
    self.labelsDiskFileSelector.showChildNodeTypes = False
    self.labelsDiskFileSelector.setMRMLScene( slicer.mrmlScene )

    optionalArgumentsLayout.addRow("Labels Disk File", self.labelsDiskFileSelector)

    self.outputFolderSelector = ctk.ctkPathLineEdit()
    self.outputFolderSelector.filters = ctk.ctkPathLineEdit.Dirs
    optionalArgumentsLayout.addRow("Output folder", self.outputFolderSelector)

    self.templateFolderSelector = ctk.ctkPathLineEdit()
    self.templateFolderSelector.filters = ctk.ctkPathLineEdit.Dirs
    self.templateFolderSelector.currentPath = self.templateFolderDefault
    optionalArgumentsLayout.addRow("Template folder", self.templateFolderSelector)

    self.contrastSelector = qt.QComboBox()
    self.contrastSelector.addItems(['','t1','t2','t2s'])
    optionalArgumentsLayout.addRow("Contrast", self.contrastSelector)

    self.registrationReferenceSelector = qt.QComboBox()
    self.registrationReferenceSelector.addItems(["template", "subject"])
    optionalArgumentsLayout.addRow("Reference for registration", self.registrationReferenceSelector)

    self.paramsSelector = qt.QTextEdit()
    optionalArgumentsLayout.addRow("Parameters for registration", self.paramsSelector)

    self.paramsstraightenSelector = qt.QTextEdit()
    optionalArgumentsLayout.addRow("Parameters for straightening", self.paramsstraightenSelector)

    self.removeTempFilesSelector = qt.QCheckBox()
    self.removeTempFilesSelector.setChecked(True)
    optionalArgumentsLayout.addRow("Remove temporary files", self.removeTempFilesSelector)

    #
    # Run button
    #

    self.runButton = qt.QPushButton("Run SCT Register To Template")
    self.runButton.enabled = False

    self.layout.addWidget(self.runButton)

    self.statusLabel = qt.QLabel("Status: ")
    self.layout.addWidget(self.statusLabel)

    #
    # Connections
    #

    self.anatomicalImageSelector.connect("currentNodeChanged(vtkMRMLNode*)",
                                         self.onSelectAnatomicalImage)
    self.segmentedImageSelector.connect("currentNodeChanged(vtkMRMLNode*)",
                                        self.onSelectSegmentedImage)
    self.runButton.connect('clicked(bool)', self.onRunButton)

  def findTemplateFolder(self):
    template_folder = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          break
      template_folder =  sct_bin_path[:sct_bin_path.rfind("/")] + "/data/PAM50/"
    except KeyError:  # path variable not found
      pass
    return template_folder

  def onSelectAnatomicalImage(self):
    anatomicalImage = self.anatomicalImageSelector.currentNode()

    if anatomicalImage:

      # check if there is a segmented image and labels file in the same directory
      anatomicalImagePath = anatomicalImage.GetStorageNode().GetFileName()
      anatomicalImageDir = anatomicalImagePath[:anatomicalImagePath.rfind("/") + 1]

      # automatically load segmented or centerline node
      if not self.segmentedImageSelector.currentNode():
        segNode = slicer.mrmlScene.GetNodesByName(anatomicalImage.GetName() + "_seg").\
                  GetItemAsObject(0)
        centerlineNode = slicer.mrmlScene.GetNodesByName(anatomicalImage.GetName() +
                                                         "_centerline").GetItemAsObject(0)
        if segNode:
          self.segmentedImageSelector.setCurrentNode(segNode)
        elif centerlineNode:
          self.segmentedImageSelector.setCurrentNode(centerlineNode)

      # load labels node
      if not self.labelsFileSelector.currentNode():
        labelsNode = slicer.mrmlScene.GetNodesByName("labels").GetItemAsObject(0)
        self.labelsFileSelector.setCurrentNode(labelsNode)

      # set contrast based on node name
      contrasts = [self.contrastSelector.itemText(i) for i in range(self.contrastSelector.count)]
      for contrast in contrasts:
        if contrast in anatomicalImage.GetName():
          idx = self.contrastSelector.findText(contrast)
          self.contrastSelector.setCurrentIndex(idx)

      # change the output folder to current directory
      if not self.outputFolderSelector.currentPath:
        self.outputFolderSelector.currentPath = anatomicalImageDir

      self.runButton.enabled = self.anatomicalImageSelector.currentNode() and \
                               self.segmentedImageSelector.currentNode()

  def onSelectSegmentedImage(self):
    segmentedNode = self.segmentedImageSelector.currentNode()
    if segmentedNode:

      self.runButton.enabled = self.anatomicalImageSelector.currentNode() and \
                               self.segmentedImageSelector.currentNode()

  def getParams(self):
    # Append mandatory options
    params = {}
    params["-i "] = self.anatomicalImageSelector.currentNode().GetStorageNode().GetFileName()
    params["-s"] = self.segmentedImageSelector.currentNode().GetStorageNode().GetFileName()

    # Append optional options
    if self.labelsFileSelector.currentNode():
      params["-l"] = self.labelsFileSelector.currentNode().GetStorageNode().GetFileName()
    if self.labelsDiskFileSelector.currentNode():
      params["-ldisk"] = self.labelsDiskFileSelector.GetStorageNode().GetFileName()
    if self.templateFolderSelector.currentPath:
      params["-t"] = self.templateFolderSelector.currentPath
    if self.outputFolderSelector.currentPath:
      params["-ofolder"] = self.outputFolderSelector.currentPath
    if self.contrastSelector.currentText:
      params["-c"] = str(self.contrastSelector.currentText)
    if not self.removeTempFilesSelector.checked:
      params["-r"] = "0"
    if str(self.paramsSelector.toPlainText()) != "":
      params["-param"] = str(self.paramsSelector.toPlainText())
    if str(self.paramsstraightenSelector.toPlainText()):
      params["-param-straighten"] = str(self.paramsstraightenSelector.toPlainText())

    return params

  def onRunButton(self):
    logic = SCTRegisterToTemplateLogic()
    params = self.getParams()
    if "-l" not in params and "-ldisk" not in params:
      self.statusLabel.text = "Status: Labels should be provided."

    else:
      statusCode = logic.run(params)
      self.statusLabel.text = self.statusDict[statusCode]

  def cleanup(self):
    pass

#
# SCTRegisterToTemplateLogic
#

class SCTRegisterToTemplateLogic(ScriptedLoadableModuleLogic):

  def run(self, params_dict):
    """calls sct_utils.run to run command

    Adds sct_scripts to path by looking at bash variables
    Returns status code:
    statusCode == 0  : sct_register_to_template ran without any error
    statusCode == 1  : sct_register_to_template ran successfully but could not load output
    statusCode == -1 : sct_register_to_template did not run because sct/scripts dir was not found
    statusCode == -2 : sct_register_to_template ran and threw some exception

    """
    statusCode = 0

    try:
      if self.addSCTScriptsToPath():

        import sct_utils as sct
        import sct_register_to_template
        from msct_parser import Parser

        params_str = Parser.dictionary_to_string(sct_register_to_template.get_parser(), params_dict)

        sct.run("sct_register_to_template " + params_str)

        # output files
        anat2tempName = "anat2template"
        temp2anatName = "template2anat"
        warp_anat2tempName = "warp_" + anat2tempName
        warp_temp2anatName = "warp_" + temp2anatName

        # remove nodes with the same name

        self.removeNodeIfExists(anat2tempName)
        self.removeNodeIfExists(temp2anatName)
        self.removeNodeIfExists(warp_anat2tempName)
        self.removeNodeIfExists(warp_temp2anatName)

        outputFolder = params_dict["-ofolder"]
        if outputFolder[-1] != "/":
          outputFolder += "/"

        nii = ".nii.gz"

        anat2tempPath = outputFolder + anat2tempName + nii
        temp2anatPath = outputFolder + temp2anatName + nii
        warp_anat2tempPath = outputFolder + warp_anat2tempName + nii
        warp_temp2anatPath = outputFolder + warp_temp2anatName + nii

        load_anat2tempPath, anat2temp = slicer.util.loadVolume(anat2tempPath, {}, True)
        load_temp2anatPath, temp2anat = slicer.util.loadVolume(temp2anatPath, {}, True)
        load_warp_anat2tempPath, warp_anat2temp = slicer.util.loadVolume(
          warp_anat2tempPath, {"show" : False}, True)
        load_warp_temp2anatPath, warp_temp2anat = slicer.util.loadVolume(
          warp_temp2anatPath, {"show" : False}, True)

        if not load_anat2tempPath: statusCode = 1
        else: anat2temp.SetName(anat2tempName)
        if not load_temp2anatPath: statusCode = 1
        else: temp2anat.SetName(temp2anatName)
        if not load_warp_anat2tempPath: statusCode = 1
        else: warp_anat2temp.SetName(warp_anat2tempName)
        if not load_warp_temp2anatPath: statusCode = 1
        else: warp_temp2anat.SetName(warp_temp2anatName)

      else:
        statusCode = -1

    except: # Prevent Slicer from crashing
      statusCode = -2
      print(sys.exc_info())

    return statusCode

  def removeNodeIfExists(self, nodeName):
    node = slicer.util.getNode(nodeName)
    if node:
      slicer.mrmlScene.RemoveNode(node)

  def addSCTScriptsToPath(self):
    '''Finds sct/bin directory in system path and adds sct/scripts to system path variable
    returns True if sct/bin was found in system path, else returns false'''

    paths = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          sct_scripts_path =  sct_bin_path[:sct_bin_path.rfind("/")] + "/scripts"
          sys.path.append(sct_scripts_path)
          return True
      return False
    except KeyError:  # path variable not found
      return False

class SCTRegisterToTemplateTest(ScriptedLoadableModuleTest):

  def setUp(self):
    slicer.mrmlScene.Clear(0)
    sys.path.append("/usr/local/Vignesh/sct/scripts")
    import sct_testing

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_SCTRegisterToTemplate()

  def test_SCTRegisterToTemplate(self):
    """Calls sct_testing to carry out the testing
    """
    self.delayDisplay("No tests avaiable")
