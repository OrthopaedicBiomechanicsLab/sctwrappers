# Spinal Cord Diffusion MRI

This custom version of 3D Slicer has an automated tractography pipeline that generates streamlines. With the incorporation of a custom version of Spinal Cord Tool Box, you can exploit all of the capabilities of Spinal Cord Tool Box with a GUI instead of being limited to Command Line functions.

-----------------------------

## Getting Started

These instructions will get you a copy of sctSlicer up and running on your local Ubuntu machine for development and testing purposes.

We have found these instructions to be difficult to setup and get working, as such we have also provided a Docker based install, please see: [SCT+Docker+SOP.htm](SCT+Docker+SOP.htm)

### Prerequisites

1. You must have Ubuntu 16.04 or 18.04.
2. You must have a compatible Python with 2.7.13.
3. You must have QT4.

```
    sudo apt-get install qt4-qmake;

    sudo apt-get install libqt4-dev;

```

NB. All of your spine data must follow the file structure and naming convention as the [template](https://queensuca-my.sharepoint.com/:u:/g/personal/15pyw_queensu_ca/EZt0-3v-w2lGnychN9_TNdgBoRhBq1fpwLaHKA2lgjvw4A?e=yMBGbT). For _inputs.json_ file, please change the sct directory path to the location of _inputs.json_ file if you are planning on testing this sample data.

### Installation

1. Download the tarball from [Slicer_Tarball_onedrive](https://queensuca-my.sharepoint.com/:u:/g/personal/15pyw_queensu_ca/EWTg9uKXDK1Bpe3eKZbx1HkB0wGQnk6fhv7JjRorxBQM3w?e=FncGPf) and extract the files to a location on your computer. This might take a few minutes or hours depending on the specs of your machine and what else you are running. To extract the files, you can right click on the tarball and click _Extract Here_ or you can run `tar -xvzf Slicer-mod-sct.tar.gz` in your Terminal.
2. Open a Command Line Terminal (Ctrl + Alt + t)
3. Export the current path of /sct/bin and the library paths as LD_LIBRARY_PATH. You will need to export these paths before each usage of sctSlicer every time the Terminal closes. Or you can append these to your ~/.bashrc file for a more permanent fix. Please follow the syntax of the following exports:
```
    export PATH='/home/sctSlicer/Slicer-mod-sct/Slicer-mod/modules/sct/bin':$PATH
```

```
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/sctSlicer/Slicer-mod-sct/Slicer-mod/Slicer-4.7.0-2017-10-11-linux-amd64/lib/Slicer-4.7/:/home/sctSlicer/Slicer-mod-sct/Slicer-mod/modules/git/SlicerDMRI/lib/Slicer-4.7/qt-loadable-modules:/home/sctSlicer/Slicer-mod-sct/Slicer-mod/Slicer-4.7.0-2017-10-11-linux-amd64/lib/Teem-1.12.0:/home/sctSlicer/Slicer-mod-sct/Slicer-mod/Slicer-4.7.0-2017-10-11-linux-amd64/lib/Python/lib:/home/sctSlicer/Slicer-mod-sct/Slicer-mod/modules/git/SlicerDMRI/lib/Slicer-4.7/cli-modules:/home/sctSlicer/Slicer-mod-sct/Slicer-mod/modules/git/ModelToModelDistance/build/lib/Slicer-4.7/cli-modules/

```

4. In the Terminal, navigate to the directory of the packaged Slicer. For example,
    ```
    cd /home/sctSlicer/Slicer-mod-sct/Slicer-mod/Slicer-4.7.0-2017-10-11-linux-amd64
    ```
5. In the Terminal, run `./Slicer`
6. Slicer should open. On the first run of Slicer, you will need to add all of the module paths.
    1. Go to the top left corner of 3D Slicer and open the **Edit** tab on the menu bar.
    2. Then open **Application Settings**.
    3. In **Application Settings**, go to **Modules** from the left side of the window.
    4. Under _Additional module paths_, hit **Add**.
    5. All of the modules should have been downloaded with the tarball. Add:

    > /home/sctSlicer/Slicer-mod/modules/git/ModelToModelDistance/build/lib/Slicer-4.7/cli-modules;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SlicerDMRI/lib/Slicer-4.7/cli-modules;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/DiceComputation/lib/Slicer-4.7/qt-loadable-modules;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SlicerDMRI/lib/Slicer-4.7/qt-loadable-modules;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SlicerDMRI/lib/Slicer-4.7/qt-scripted-modules;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SCTWrappers/SCTConcatTransfo;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SCTWrappers/SCTDMRISeparateB0DWI;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SCTWrappers/SCTLabelUtils;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SCTWrappers/SCTLabelVertebrae;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SCTWrappers/SCTPropseg;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SCTWrappers/SCTRegisterMultimodal;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SCTWrappers/SCTRegisterToTemplate;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SCTWrappers/SCTTractographyPipeline;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/SCTWrappers/SCTWarpTemplate;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/tractography/CreateSeedingROI;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/tractography/Streamline;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/tractography/StreamlineToTractDistance;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/tractography/StreamlineVerification;
    >
    > /home/sctSlicer/Slicer-mod/modules/git/tractography/TractographyPipeline;

    6. Click **Okay** to restart Slicer.

7. sctSlicer should now be usable and you can proceed to the next section.

-----------------------------

## Usage

NB: many of the commands take some time to run and may freeze the GUI.


### Running the SCT wrappers

Information about the parameters for the SCT Wrappers can be found [here](https://sourceforge.net/p/spinalcordtoolbox/wiki/Home/)

#### SCT Propseg

1. Select the _input volume_ from the drop-down box.
2. Select the _desired contrast_ from the drop-down box. If the input volume contains the contrast name (e.g. t1.nii.gz) the corresponding contrast (t1) will be automatically selected.
3. Set the segmentation parameters. More information about the segmentation parameters can be found on the SCT wiki.
4. Click **Run SCT Propseg** button

#### SCT Label Vertebrae

1. Select the _input volume_ from the drop-down box.
2. Select the _segmented volume_ from the drop-down box. If the segmented image was obtained using SCT and the segmented image was loaded correctly, it will be selected automatically.
3. Select the _desired contrast_ from the drop-down box. If the input volume contains the contrast name (e.g. t1.nii.gz) the corresponding contrast (t1) will be automatically selected.
4. Select the various parameters for the labelling. More information about the parameters can be found on the SCT wiki.
5. Click **Run SCT Label Vertebrae**

#### SCT Label Utils

1. Select the _labelled volume_ from the drop-down box.
2. Select a process to carry out. For the SCT Tractography pipeline, a labels file needs to be created for vertebral levels. To select vertebral levels, type the comma-separated levels into the text box (e.g. 2,6 for levels 2 to 6). More information about the other process can be found in the SCT wiki.
3. Click **Run SCT Label Utils**

#### SCT Register To Template

1. Select the _anatomical image_ and _segmented image_ from the drop-down boxes.
2. Select the _labels file_ from the drop-down box
3. The parameters for registration can be modified using the format specified in the SCT wiki.
4. Click **Run SCT Register To Template** button

#### SCT Warp Template

1. Select the _destination image_ and the _warping fields_ from the drop-down boxes
2. Click **Run SCT Warp Template**

#### SCT DMRI Separate B0 DWI

1. Select the _diffusion data_ from the drop down menu
2. Select the paths to the bvecs file and the bvals files from the menu. If the bvecs and bvals file exist in the same directory as the diffusion data, these will be selected automatically
3. Click **Run SCT DMRI Separate B0 DWI**

#### SCT Register Multimodal

1. Select the _image source volume_ and the _image destination volumes_ from the drop down menu
2. Select the various general options from the menu. More information about the general options can be found in the SCT wiki. Note that when running this module with the parameters used in the SCT pipeline, it is necessary to load the PAM50 template files into Slicer in order to select them.
3. The parameters for registration can be set for each step of the registration. The parameters can be specified either by entering the raw parameters in the text box as defined in the SCT wiki or through the GUI. To specify parameters for a step, click on **Add Step**. Modify the parameters as needed and click on Set parameters to add the parameters for that step. The parameters will be shown on the textbox. Clicking on **close** does not add the parameters to the parameter list and will not add the parameters to the text box. Parameter steps can be removed by clicking **Remove Last Step**. To facilitate the proper selection of parameters, some values cannot be modified unless other values are selected. More information about these parameters can also be found on the GUI.
4. Click **Run SCT Register Multimodal**

#### SCT Concat Transfo

1. Select the _destination image_ from the drop-down menu
2. Modify the number of images that will be concatenated. Changing the number of images changes the number of fields available to add images accordingly.
3. Select the _warp images_ from the drop-down lists
4. Select the _output warp field directory_ using the directory selector.

### Running the SCT Tractography Pipeline

NB: It takes around 20 to 30 minutes to run the whole pipeline on our computer system. So please do not be alarmed if it seems to be taking an awfully long time.

#### SCT Tractography Pipeline

1. Select the weighted image and the dmri image from the drop-down lists provided.
2. There's currently a bug in which the only way to run everything is to uncheck the **Run Tractography Pipeline** option. Otherwise in normal functioning, the **Run Tractography Pipeline** option can be unchecked if the running of the tractography pipeline is not desired.
3. Parameters for each step of the pipeline can be modified by clicking on **Options**. This opens a pop-up window with a list of each step and the user interface for each step.
4. For all of the SCT steps, the UI is exactly the same as that for the corresponding SCT wrapper. The final step has a custom UI that defines fields for most of the parameters that will be used in the json file that will be generated for the tractography pipeline. To save modifications, either close the window or click the Apply button.
5. Click **Run SCT Tractography Pipeline**


### Running the Tractography Module

NB: It takes around 10 to 15 minutes to run the whole pipeline on our computer system. So please do not be alarmed if it seems to be taking an awfully long time.

#### CreateSeedingROI

1. Select the SCT Directory from the button with the 3 periods and select tract volume(s) in the checklist that appears for which you would like to seed. Then click **Import SCT Files**.
2. Verify that the _Number of Tracts_, _Tract Volume_, _Tract Threshold_, and _Levels Volume_ are correct. If everything is correct, they should load automatically.
3. Click **Apply**

#### Streamline

NB: If DWI errors occur during Streamlines Module, please close Slicer and restart. You files should have already saved.

1. Select the SCT Directory from the button with the 3 periods, then click **Import SCT Files**.
2. In the drop down menu for _Seeding ROI Volume_, select the atlas volume you would like to seed. Then select the seeding labels from the checklist below.
3. Adjust the following settings to your desire.
4. Then click **Run Tractography Seeding** and repeat the above steps as necessary for each volume that needs to be seeded.

#### Streamline to Tract Distance

1. Select the SCT Directory from the button with the 3 periods, then select the streamline from the checklist below.
2. Click **Import SCT Files**
3. Verify that the _Streamline_ atlas selection is correct. If everything is correct, they should load automatically.
4. Click **Apply**

#### Streamline Verfication

1. Select the SCT Directory from the button with the 3 periods, then select the streamline from the checklist below.
2. Click **Import SCT Files**
3. Verify that the _Streamline_ atlas selection, _Target Grid Volume_, _Target Grid Spacing_, _Tract Volume_, and _ROI Volume_ are correct. If everything is correct, they should load automatically.
4. You can check or uncheck _Keep Clipped Volumes_ if you would like to save the Clipped Volumes.
5. Select the ROI from the checklist below.
6. Click **Apply**

#### Tractography Pipeline

1. Input a JSON File that automates the whole pipeline from your SCT Directory.
2. Click **Apply**
3. In the file directory, a new folder names _slicer-output_ is generated. Inside, you will find a folder named _results_ in which .csv files are created with all of the calculated results of the Tractography Pipeline.

    -----------------------------

## Development

### Built with
- [3D Slicer](https://www.slicer.org/) - We made a custom build with the integration of ANTs
- [ANTs](https://sourceforge.net/projects/advants/) - A prerequisite needed to build Spinal Cord Tool Box
- [Docker](https://www.docker.com/) - Was used to create the project and the binaries
- [Spinal Cord Tool Box](https://sourceforge.net/projects/spinalcordtoolbox/) - Slicer provides the GUI for Spinal Cord Tool Box (we used a custom version)

### Contributing



### License



### Acknowledgements



### Project Status

In development.
