"""
Code for the Options Dialog and tractography options for the SCT Tractography Pipeline
"""

import ast
import vtk, qt, ctk, slicer

class OptionsDialog(qt.QDialog):

  def __init__(self, propsegWeighted, propsegdwi, labelVertebrae, labelUtils, registerToTemplate,
               warpTemplate, dmriseparateb0dwi, registerMultimodal, concatTransfo, runTractography):

    #
    # Segmentation of the weighted image
    #

    qt.QDialog.__init__(self)
    self.setWindowTitle("Options for SCT Tractography Pipeline")
    dialogLayout = qt.QVBoxLayout()
    formsLayout = qt.QHBoxLayout()

    self.stackedWidget = qt.QStackedWidget()
    self.formsList = qt.QListWidget()
    listLayout = qt.QVBoxLayout()

    formsLayout.addLayout(listLayout)
    formsLayout.addWidget(self.stackedWidget)
    dialogLayout.addLayout(formsLayout)
    self.setLayout(dialogLayout)

    self.formsList.addItem("Segment the weighted image")
    self.formsList.addItem("Label the weighted image")
    self.formsList.addItem("Generate Labels for specific vertebral levels")
    self.formsList.addItem("Register weighted image to PAM50")
    self.formsList.addItem("Warp PAM50 to weighted image")
    self.formsList.addItem("Separate b0, dwi")
    self.formsList.addItem("Segment dmri image")
    self.formsList.addItem("Register dwi to PAM50")
    self.formsList.addItem("Concatenate transforms")
    self.formsList.addItem("Tractography Pipeline options")

    self.formsList.connect("currentItemChanged(QListWidgetItem *, QListWidgetItem *)",
                           self.updateForm)

    listLayout.addWidget(qt.QLabel("Steps:"))
    listLayout.addWidget(self.formsList)

    #
    # Propseg of the weighted image
    #

    propseg1Tab = qt.QWidget()
    propseg1Layout = qt.QVBoxLayout()

    propsegWeighted.self().developerMode = False
    scrollArea = qt.QScrollArea()
    scrollArea.widgetResizable = True
    scrollArea.setWidget(propsegWeighted)
    propseg1Layout.addWidget(scrollArea)

    propseg1Tab.setLayout(propseg1Layout)
    self.stackedWidget.addWidget(propseg1Tab)

    #
    # Label Vertebrae
    #

    labelVertebraeTab = qt.QWidget()
    labelVertebraeLayout = qt.QVBoxLayout()

    labelVertebrae.self().developerMode = False
    scrollArea = qt.QScrollArea()
    scrollArea.widgetResizable = True
    scrollArea.setWidget(labelVertebrae)
    labelVertebraeLayout.addWidget(scrollArea)

    labelVertebraeTab.setLayout(labelVertebraeLayout)
    self.stackedWidget.addWidget(labelVertebraeTab)

    #
    # Label utils
    #

    labelUtilsTab = qt.QWidget()
    labelUtilsLayout = qt.QVBoxLayout()

    labelUtils.self().developerMode = False
    scrollArea = qt.QScrollArea()
    scrollArea.widgetResizable = True
    scrollArea.setWidget(labelUtils)
    labelUtilsLayout.addWidget(scrollArea)

    labelUtilsTab.setLayout(labelUtilsLayout)
    self.stackedWidget.addWidget(labelUtilsTab)

    #
    # Register to template
    #

    registerToTemplateTab = qt.QWidget()
    registerToTemplateLayout = qt.QVBoxLayout()

    registerToTemplate.self().developerMode = False
    scrollArea = qt.QScrollArea()
    scrollArea.widgetResizable = True
    scrollArea.setWidget(registerToTemplate)
    registerToTemplateLayout.addWidget(scrollArea)

    registerToTemplateTab.setLayout(registerToTemplateLayout)
    self.stackedWidget.addWidget(registerToTemplateTab)

    #
    # Warp Template
    #

    warpTemplateTab = qt.QWidget()
    warpTemplateLayout = qt.QVBoxLayout()

    warpTemplate.self().developerMode = False
    scrollArea = qt.QScrollArea()
    scrollArea.widgetResizable = True
    scrollArea.setWidget(warpTemplate)
    warpTemplateLayout.addWidget(scrollArea)

    warpTemplateTab.setLayout(warpTemplateLayout)
    self.stackedWidget.addWidget(warpTemplateTab)

    #
    # DMRI Separate b0 dwi
    #

    dmriSeparateb0dwiTab = qt.QWidget()
    dmriSeparateb0dwiLayout = qt.QVBoxLayout()

    dmriseparateb0dwi.self().developerMode = False
    scrollArea = qt.QScrollArea()
    scrollArea.widgetResizable = True
    scrollArea.setWidget(dmriseparateb0dwi)
    dmriSeparateb0dwiLayout.addWidget(scrollArea)

    dmriSeparateb0dwiTab.setLayout(dmriSeparateb0dwiLayout)
    self.stackedWidget.addWidget(dmriSeparateb0dwiTab)

    #
    # Propseg of the dmri image
    #

    propseg2Tab = qt.QWidget()
    propseg2Layout = qt.QVBoxLayout()

    propsegdwi.self().developerMode = False
    scrollArea = qt.QScrollArea()
    scrollArea.widgetResizable = True
    scrollArea.setWidget(propsegdwi)
    propseg2Layout.addWidget(scrollArea)

    propseg2Tab.setLayout(propseg2Layout)
    self.stackedWidget.addWidget(propseg2Tab)

    #
    # Register multimodal
    #

    registermultimodalTab = qt.QWidget()
    registermultimodalLayout = qt.QVBoxLayout()

    registerMultimodal.self().developerMode = False
    scrollArea = qt.QScrollArea()
    scrollArea.widgetResizable = True
    scrollArea.setWidget(registerMultimodal)
    registermultimodalLayout.addWidget(scrollArea)

    registermultimodalTab.setLayout(registermultimodalLayout)
    self.stackedWidget.addWidget(registermultimodalTab)

    #
    # Concatenate transforms
    #

    concattransfoTab = qt.QWidget()
    concattransfoLayout = qt.QVBoxLayout()

    concatTransfo.self().developerMode = False
    scrollArea = qt.QScrollArea()
    scrollArea.widgetResizable = True
    scrollArea.setWidget(concatTransfo)
    concattransfoLayout.addWidget(scrollArea)

    concattransfoTab.setLayout(concattransfoLayout)
    self.stackedWidget.addWidget(concattransfoTab)

    #
    # Tractography pipeline options
    #

    self.stackedWidget.addWidget(runTractography)

    #
    # Apply button
    #

    applyButton = qt.QPushButton("Apply")
    dialogLayout.addWidget(applyButton)
    applyButton.connect('clicked(bool)', self.closeDialog)

  def updateForm(self):
    self.stackedWidget.setCurrentIndex(self.formsList.currentRow)

  def closeDialog(self):
    self.done(0)

class TractographyPipelineForm(qt.QWidget):
  """ This class provides fields for modifying all of the fields
  that are specified in the JSON file for the tractography pipeline. It also
  generates json files that can be passed to the tractography pipeline under the hood
  """

  def __init__(self):

    # sct defaults
    tractDefault = ["atlas_4", "atlas_5", "atlas_22", "atlas_23", "template_3"]
    levelsDefault = ""

    # streamline defaults
    tractThresholdDefault = 0.3
    fittingDefault = "WLS"
    shiftNegativeEigenValuesDefault = False
    startThresholdDefault = 0.3
    minLengthDefault = 10.0
    maxLengthDefault = 800.0
    stoppingModeValues = ["Fractional Anisotropy", "Linear Measure"]
    stoppingModeDefault = stoppingModeValues[0]
    stoppingValueDefault = 0.15
    stoppingTrackCurvatureDefault = 0.7
    integrationStepLengthDefault = 0.5
    useIndexSpaceDefault = False
    seedSpacingDefault = 0.5
    randomGridDefault = False
    targetLabelsDefault = [[4],[4,5],[3,4,5]]
    seedLabelsDefault = [[3,5],[3,6],[2,6]]
    resampleSpacingDefault = "0.5,0.5,0.5"
    extractStreamlinesInROIDefault = True
    createSeedingROIDefault = True
    generateStreamlineDefault = True
    verifyStreamlineDefault = True
    labelStatisticsDefault = True

    qt.QWidget.__init__(self)

    layout = qt.QVBoxLayout()
    self.setLayout(layout)
    #
    # SCT Group
    #

    sctGroupBox = qt.QGroupBox("SCT Options")
    sctLayout = qt.QFormLayout()
    sctGroupBox.setLayout(sctLayout)

    self.tractSelector = qt.QLineEdit()
    self.tractSelector.text = tractDefault
    sctLayout.addRow("Tracts: ", self.tractSelector)

    self.levelsSelector = qt.QLineEdit()
    self.levelsSelector.text = levelsDefault
    sctLayout.addRow("Levels: ", self.levelsSelector)

    #
    # Streamline Group
    #

    streamlineGroupBox = qt.QGroupBox("Streamline")
    streamlineLayout = qt.QFormLayout()
    streamlineGroupBox.setLayout(streamlineLayout)

    self.tractThresholdSelector = ctk.ctkDoubleSpinBox()
    self.tractThresholdSelector.value = tractThresholdDefault
    streamlineLayout.addRow("Tract threshold", self.tractThresholdSelector)

    self.fittingSelector = qt.QComboBox()
    self.fittingSelector.addItems(["LS","WLS"])
    idx = self.fittingSelector.findText(fittingDefault)
    self.fittingSelector.setCurrentIndex(idx)
    streamlineLayout.addRow("Fitting", self.fittingSelector)

    self.shiftNegativeEigenValues = qt.QCheckBox()
    self.shiftNegativeEigenValues.checked = shiftNegativeEigenValuesDefault
    streamlineLayout.addRow("Shift negative eigenvalues", self.shiftNegativeEigenValues)

    self.startThresholdSelector = ctk.ctkSliderWidget()
    self.startThresholdSelector.singleStep = 0.1
    self.startThresholdSelector.minimum = 0.0
    self.startThresholdSelector.maximum = 1.0
    self.startThresholdSelector.value = startThresholdDefault
    streamlineLayout.addRow("Start threshold", self.startThresholdSelector)

    self.minLengthSelector = ctk.ctkSliderWidget()
    self.minLengthSelector.singleStep = 1
    self.minLengthSelector.minimum = 0.0
    self.minLengthSelector.maximum = 200.0
    self.minLengthSelector.value = minLengthDefault
    streamlineLayout.addRow("Minimum Length (mm)", self.minLengthSelector)

    self.maxLengthSelector = ctk.ctkSliderWidget()
    self.maxLengthSelector.singleStep = 1
    self.maxLengthSelector.minimum = 0.0
    self.maxLengthSelector.maximum = 2000.0
    self.maxLengthSelector.value = maxLengthDefault
    streamlineLayout.addRow("Maximum Length (mm)", self.maxLengthSelector)

    self.stoppingModeSelector = qt.QComboBox()
    self.stoppingModeSelector.addItems(stoppingModeValues)
    idx = self.stoppingModeSelector.findText(stoppingModeDefault)
    self.stoppingModeSelector.setCurrentIndex(idx)
    streamlineLayout.addRow("Stopping mode", self.stoppingModeSelector)

    self.stoppingValueSelector = ctk.ctkSliderWidget()
    self.stoppingValueSelector.singleStep = 0.01
    self.stoppingValueSelector.minimum = 0.0
    self.stoppingValueSelector.maximum = 1.0
    self.stoppingValueSelector.value = stoppingValueDefault
    streamlineLayout.addRow("Stopping value", self.stoppingValueSelector)

    self.stoppingTrackCurvatureSelector = ctk.ctkSliderWidget()
    self.stoppingTrackCurvatureSelector.singleStep = 0.1
    self.stoppingTrackCurvatureSelector.minimum = 0.0
    self.stoppingTrackCurvatureSelector.maximum = 10.0
    self.stoppingTrackCurvatureSelector.value = stoppingTrackCurvatureDefault
    streamlineLayout.addRow("Stopping Track Curvature", self.stoppingTrackCurvatureSelector)

    self.integrationStepLengthSelector = ctk.ctkSliderWidget()
    self.integrationStepLengthSelector.singleStep = 0.1
    self.integrationStepLengthSelector.minimum = 0.0
    self.integrationStepLengthSelector.maximum = 10.0
    self.integrationStepLengthSelector.value = integrationStepLengthDefault
    streamlineLayout.addRow("Integration step length", self.integrationStepLengthSelector)

    self.useIndexSpace = qt.QCheckBox()
    self.useIndexSpace.checked = useIndexSpaceDefault
    streamlineLayout.addRow("Use index space", self.useIndexSpace)

    self.seedSpacingSelector = ctk.ctkSliderWidget()
    self.seedSpacingSelector.singleStep = 0.1
    self.seedSpacingSelector.minimum = 0.1
    self.seedSpacingSelector.maximum = 10.0
    self.seedSpacingSelector.value = seedSpacingDefault
    streamlineLayout.addRow("Seed spacing", self.seedSpacingSelector)

    self.randomGrid = qt.QCheckBox()
    self.randomGrid.checked = randomGridDefault
    streamlineLayout.addRow("Random grid", self.randomGrid)

    self.targetLabelsSelector = qt.QLineEdit()
    self.targetLabelsSelector.setText(str(targetLabelsDefault))
    streamlineLayout.addRow("Target Labels", self.targetLabelsSelector)

    self.seedLabelsSelector = qt.QLineEdit()
    self.seedLabelsSelector.setText(str(seedLabelsDefault))
    streamlineLayout.addRow("Seed Labels", self.seedLabelsSelector)

    self.resampleSpacingSelector = qt.QLineEdit()
    self.resampleSpacingSelector.setText(str(resampleSpacingDefault))
    streamlineLayout.addRow("Target Labels", self.resampleSpacingSelector)

    self.extractStreamlinesInROI = qt.QCheckBox()
    self.extractStreamlinesInROI.checked = extractStreamlinesInROIDefault
    streamlineLayout.addRow("Shift negative eigenvalues", self.extractStreamlinesInROI)

    runGroupBox = qt.QGroupBox("Run parameters")

    runLayout = qt.QFormLayout()
    runGroupBox.setLayout(runLayout)

    self.createSeedingROI = qt.QCheckBox()
    self.createSeedingROI.checked = createSeedingROIDefault
    runLayout.addRow("Create seeding ROI", self.createSeedingROI)

    self.generateStreamline = qt.QCheckBox()
    self.generateStreamline.checked = generateStreamlineDefault
    runLayout.addRow("Generate streamlines", self.generateStreamline)

    self.verifyStreamline = qt.QCheckBox()
    self.verifyStreamline.checked = verifyStreamlineDefault
    runLayout.addRow("Verify Streamlines", self.verifyStreamline)

    self.labelStatistics = qt.QCheckBox()
    self.labelStatistics.checked = labelStatisticsDefault
    runLayout.addRow("Calculate label statistics", self.labelStatistics)

    layout.addWidget(sctGroupBox)
    layout.addWidget(streamlineGroupBox)
    layout.addWidget(runGroupBox)

  def getParamsDict(self):
    """
    Returns a dictionary with all of values for each parameter of the pipeline
    Does not contain field for sctDirectory, bval, bvecs.
    To create json file from dict for to run pipeline, these values need to be added
    to the sct dictionary
    """
    sctDict = {}
    sctDict["tract"] = ast.literal_eval(self.tractSelector.text)
    sctDict["levels"] = self.levelsSelector.text

    streamlineDict = {}
    streamlineDict["tractThreshold"] = self.tractThresholdSelector.value
    streamlineDict["fitting"] = self.fittingSelector.currentText
    streamlineDict["shiftNegativeEigenValues"] = self.shiftNegativeEigenValues.isChecked()
    streamlineDict["startThreshold"] = self.startThresholdSelector.value
    streamlineDict["minLength"] = self.minLengthSelector.value
    streamlineDict["maxLength"] = self.maxLengthSelector.value
    streamlineDict["stoppingMode"] = self.stoppingModeSelector.currentText
    streamlineDict["stoppingValue"] = self.stoppingValueSelector.value
    streamlineDict["stoppingTrackCurvature"] = self.stoppingTrackCurvatureSelector.value
    streamlineDict["integrationStepLength"] = self.integrationStepLengthSelector.value
    streamlineDict["useIndexSpace"] = self.useIndexSpace.isChecked()
    streamlineDict["seedSpacing"] = self.seedSpacingSelector.value
    streamlineDict["randomGrid"] = self.randomGrid.isChecked()
    streamlineDict["targetLabels"] = ast.literal_eval(self.targetLabelsSelector.text)
    streamlineDict["seedLabels"] = ast.literal_eval(self.seedLabelsSelector.text)
    streamlineDict["resampleSpacing"] = self.resampleSpacingSelector.text
    streamlineDict["extractStreamlinesInROI"] = self.extractStreamlinesInROI.isChecked()

    runDict = {}
    runDict["createSeedingROI"] = self.createSeedingROI.isChecked()
    runDict["generateStreamline"] = self.generateStreamline.isChecked()
    runDict["verifyStreamline"] = self.verifyStreamline.isChecked()
    runDict["labelStatistics"] = self.labelStatistics.isChecked()

    tractDict = {"sct": sctDict,
                 "streamline": streamlineDict,
                 "run": runDict}

    return tractDict
