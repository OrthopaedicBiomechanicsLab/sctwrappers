import sys
import os
import json
from resources.TractographyPipelineGUI import OptionsDialog, TractographyPipelineForm

import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import SimpleITK as sitk
import sitkUtils

import SCTPropseg
import SCTLabelVertebrae
import SCTLabelUtils
import SCTRegisterToTemplate
import SCTWarpTemplate
import SCTDMRISeparateB0DWI
import SCTRegisterMultimodal
import SCTConcatTransfo
import TractographyPipeline

#
# SCTTractographyPipeline
#

class SCTTractographyPipeline(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "SCT Tractography Pipeline"
    self.parent.categories = ["Spinal Cord Toolbox"]
    self.parent.contributors = ["Vignesh Sivan (Sunnybrook)"]
    self.parent.helpText = """"""  # TODO
    self.parent.acknowledgementText = ""

#
# SCTTractographyPipelineWidget
#

class SCTTractographyPipelineWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # default values
    self.defaults = {}
    self.defaults["vertebralBodyLevels"] = "2,6"
    self.defaults["multimodalRegParams"] = "step=1,type=seg,algo=slicereg,poly=5,smooth=5"

    # initialize spinal cord toolbox widgets and options dialog

    self.initializeSCTWidgets()
    self.tractographyForm = TractographyPipelineForm()


    # the widgets are passed into the options dialog and will be used when running the pipeline
    self.optionsDialog = OptionsDialog(self.propsegWeighted, self.propsegdwi, self.labelVertebrae,
                                       self.labelUtils, self.registerToTemplate, self.warpTemplate,
                                       self.dmriseparateb0dwi, self.registerMultimodal,
                                       self.concatTransfo, self.tractographyForm)

    #
    # Main Input Area
    #

    inputCollapsibleButton = ctk.ctkCollapsibleButton()
    inputCollapsibleButton.text = "Input"
    self.layout.addWidget(inputCollapsibleButton)

    inputLayout = qt.QFormLayout(inputCollapsibleButton)

    self.weightedImageSelector = slicer.qMRMLNodeComboBox()
    self.weightedImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.weightedImageSelector.selectNodeUponCreation = True
    self.weightedImageSelector.addEnabled = False
    self.weightedImageSelector.removeEnabled = True
    self.weightedImageSelector.noneEnabled = True
    self.weightedImageSelector.showHidden = False
    self.weightedImageSelector.showChildNodeTypes = False
    self.weightedImageSelector.setMRMLScene( slicer.mrmlScene )

    inputLayout.addRow("Weighted MRI image", self.weightedImageSelector)

    self.dmriImageSelector = slicer.qMRMLNodeComboBox()
    self.dmriImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.dmriImageSelector.selectNodeUponCreation = True
    self.dmriImageSelector.addEnabled = False
    self.dmriImageSelector.removeEnabled = True
    self.dmriImageSelector.noneEnabled = True
    self.dmriImageSelector.showHidden = False
    self.dmriImageSelector.showChildNodeTypes = False
    self.dmriImageSelector.setMRMLScene( slicer.mrmlScene )

    inputLayout.addRow("Diffusion-weighted MRI image", self.dmriImageSelector)

    # Run tractography pipeline
    self.runTractrographySelector = qt.QCheckBox()
    self.runTractrographySelector.checked = True
    inputLayout.addRow("Run tractography pipeline", self.runTractrographySelector)

    # Buttons

    buttonsLayout = qt.QHBoxLayout()

    self.runButton = qt.QPushButton("Run Pipeline")
    self.runButton.enabled = False

    self.optionsButton = qt.QPushButton("Options")
    self.optionsButton.enabled = True

    buttonsLayout.addWidget(self.runButton)
    buttonsLayout.addWidget(self.optionsButton)

    inputLayout.addRow(buttonsLayout)

    # Status

    self.statusLabel = qt.QLabel("Status: ")
    inputLayout.addRow(self.statusLabel)

    #
    # Connections
    #

    self.runButton.connect('clicked(bool)', self.onRunButton)
    self.weightedImageSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelectInputImage)
    self.dmriImageSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelectDMRIImage)
    self.optionsButton.connect('clicked(bool)', self.openOptionsDialog)

  def onSelectInputImage(self):
    """The input image in propseg widget representation should be modified
    when the input image is modified
    """
    inputNode = self.weightedImageSelector.currentNode()
    self.propsegWeighted.self().inputImageSelector.setCurrentNode(inputNode)

    if inputNode and self.dmriImageSelector.currentNode():
      self.runButton.enabled = True

  def onSelectDMRIImage(self):
    """The dmri image in propseg widget representation should be modified
    when the dmri image is modified
    """
    dmriNode = self.dmriImageSelector.currentNode()
    self.dmriseparateb0dwi.self().diffusionDataSelector.setCurrentNode(dmriNode)

    if dmriNode and self.weightedImageSelector.currentNode():
      self.runButton.enabled = True

  def onRunButton(self):
    """Runs all sct commands in the pipeline and optionally the tractography pipeline
    Updates the status
    """
    logic = SCTTractographyPipelineLogic()

    self.statusLabel.text = logic.run(self.statusLabel, self.propsegWeighted.self(),
                                      self.propsegdwi.self(), self.labelVertebrae.self(),
                                      self.labelUtils.self(), self.registerToTemplate.self(),
                                      self.warpTemplate.self(), self.dmriseparateb0dwi.self(),
                                      self.registerMultimodal.self(), self.concatTransfo.self(),
                                      self.tractographyForm
                                      if self.runTractrographySelector.isChecked()
                                      else None)

  def initializeSCTWidgets(self):
    """Initializes the widgets that will be used to run the pipeline
    The widgets are initialized as class variables, so that changes to
    parameters in options can be kept track of
    """
    self.propsegWeighted  = slicer.modules.sctpropseg.createNewWidgetRepresentation()
    self.propsegdwi = slicer.modules.sctpropseg.createNewWidgetRepresentation()
    self.labelVertebrae = slicer.modules.sctlabelvertebrae.createNewWidgetRepresentation()
    self.labelUtils = slicer.modules.sctlabelutils.createNewWidgetRepresentation()
    self.registerToTemplate = slicer.modules.sctregistertotemplate.createNewWidgetRepresentation()
    self.warpTemplate = slicer.modules.sctwarptemplate.createNewWidgetRepresentation()
    self.dmriseparateb0dwi = slicer.modules.sctdmriseparateb0dwi.createNewWidgetRepresentation()
    self.registerMultimodal = slicer.modules.sctregistermultimodal.createNewWidgetRepresentation()
    self.concatTransfo = slicer.modules.sctconcattransfo.createNewWidgetRepresentation()

    # default params for widgets
    self.labelUtils.self().vertbodySelector.text = self.defaults["vertebralBodyLevels"]
    self.registerMultimodal.self().paramsForReg.insertPlainText(
        self.defaults["multimodalRegParams"])

  def openOptionsDialog(self):
    self.optionsDialog.exec_()

  def cleanup(self):
    pass

#
# SCTTractographyPipelineLogic
#

class SCTTractographyPipelineLogic(ScriptedLoadableModuleLogic):

  def run(self, label, propsegWeighted,propsegdwi, labelVertebrae, labelUtils,
          registerToTemplate, warpTemplate, dmriseparateb0dwi, registerMultimodal, concatTransfo,
          runTractographySelector):
    """Runs the spinalcordtoolbox pipeline synchronously. The widgets are passed because they
    contain the references to the arguments that will be used to run the pipeline. If inputJSON
    is not none, the tractography pipeline will not be run
    """
    status = "Status: "

    try:

      sctPath = self.getSCTPath()
      if not sctPath:
        return "Could not find sct/bin in path variable"

      weightedImage = propsegWeighted.inputImageSelector.currentNode()

      # segment the spinal cord
      status, weightedImageSeg = self.propseg(weightedImage, propsegWeighted)

      if not weightedImageSeg:
        return status
      else: label.text = status

      # label vertebrae
      status, weightedImageSegLabeled = self.labelVertebrae(weightedImage,
                                                            weightedImageSeg, labelVertebrae)

      if not weightedImageSegLabeled:
        return status
      else: label.text = status

      # get two label points
      status, labels = self.labelUtils(weightedImageSegLabeled, labelUtils)

      if not labels:
        return status
      else: label.text = status

      # register weighted image to PAM50
      status, warpFields = self.registerToTemplate(weightedImage, weightedImageSeg,
                                                   labels, registerToTemplate)

      if not warpFields:
        return status
      else: label.text = status

      # warp PAM50 to weighted image
      # Note: no need for WM atlas at this point (-a 0)

      statusCode, status = self.warpTemplate(weightedImage, warpFields[0], warpTemplate)

      if statusCode != 0:
        return status
      else: label.text = status

      # separate b=0 and DWI and average each group
      status, dwi_mean = self.dmriSeparateB0DWI(dmriseparateb0dwi)

      if not dwi_mean:
        return status
      else: label.text = status

      # segment cord

      status, dwi_meanSeg = self.propseg(dwi_mean, propsegdwi)

      if not dwi_meanSeg:
        return status
      else: label.text = status

      # register dmri template to dmri
      # first load templates, if they aren't already loaded
      weightedAtlasName = "PAM50_" + propsegWeighted.contrastSelector.currentText
      atlasCordName = "PAM50_cord"
      nii = ".nii.gz"

      sctDataPath = sctPath + "data/PAM50/template/"

      loadedWeightedAtlas, weightedAtlas = self.loadNodeFromSceneOrFile(weightedAtlasName,
                                                                        sctDataPath +
                                                                        weightedAtlasName + nii)

      loadedAtlasCord, atlasCord = self.loadNodeFromSceneOrFile(atlasCordName, sctDataPath +
                                                                atlasCordName + nii)

      if not loadedWeightedAtlas or not loadedAtlasCord:
        return "Could not load the template files"

      status, outputWarp = self.registerMultimodal(dwi_mean, weightedAtlas, dwi_meanSeg,
                                                   atlasCord, warpFields[1], registerMultimodal)

      if not outputWarp:
        return status
      else: label.text = status

      # concatenate transforms

      statusCode, status = self.concatTransfo(weightedImage,
                                              (outputWarp, warpFields[0]), concatTransfo)

      if statusCode != 0:
        return status
      else: label.text = status

      # run the tractography pipeline
      if runTractographySelector:

        inputJSON = self.getTractographyJSON(runTractographySelector, dwi_mean)
        print(inputJSON)

        label.text = "Status: Running the tractography pipeline"
        # get the input json from options menu
        status, outputs = self.runTractographyPipeline(inputJSON)

        if not outputs:
          return status

      status = "SCT Tractography Pipeline ran successfully"
      return status

    except:  # Prevent Slicer from crashing in case of an error
      status = "SCT Tractography Pipeline failed."
      print(sys.exc_info())
      return status

  def getTractographyJSON(self, tractographySelector, dwi_mean):
    """
    Generates a JSON file using parameters from the tractographySelector form and
    adds the additional required parameters using the dwi_mean and warpDMRItoAnat nodes
    returns the path to the json file generated
    """
    tractographyDict = tractographySelector.getParamsDict()
    dwi_mean_path = dwi_mean.GetStorageNode().GetFileName()
    sctPath = dwi_mean_path[:dwi_mean_path.rfind("/", 0, dwi_mean_path.rfind("/"))]

    tractographyDict["sct"]["sctDirectory"] = sctPath
    tractographyDict["sct"]["dmri"] = ""
    tractographyDict["sct"]["bval"] = ""
    tractographyDict["sct"]["bvec"] = ""
    tractographyDict["sct"]["dmri_mean"] = dwi_mean_path
    tractographyDict["sct"]["dmri_mean2t1"] = sctPath + "/dmri/warp_dmri2anat.nii.gz"

    inputsJSONFile = os.getcwd() + "/inputs.json"

    with open(inputsJSONFile, "w+") as f:
      json.dump(tractographyDict, f)

    return inputsJSONFile

  def removeNodeIfExists(self, nodeName):
    node = slicer.util.getNode(nodeName)
    if node:
      slicer.mrmlScene.RemoveNode(node)

  def propseg(self, image, sctPropseg):
    """calls SCTPropseg and carries out the segmentation on the given image
    returns status from SCTPropseg module and the segmented image
    """

    if image:
      sctPropseg.inputImageSelector.setCurrentNode(image)

    sctPropseg.onSelectInputImage()
    params = sctPropseg.getParams()
    propsegLogic = SCTPropseg.SCTPropsegLogic()
    statusCode = propsegLogic.run(params)

    image_seg = None

    if statusCode == 0:
      image_seg = slicer.mrmlScene.GetNodesByName(image.GetName() + "_seg").GetItemAsObject(0)

    return sctPropseg.statusDict[statusCode], image_seg

  def labelVertebrae(self, image, segmentedImage, sctLabelVertebrae):
    """calls sct_label_vertebrae that labels the vertebrae
    returns status label from SCTLabelVertebrae and the labeled segmented image
    """
    sctLabelVertebrae.inputImageSelector.setCurrentNode(image)
    sctLabelVertebrae.onSelectInputImage()
    sctLabelVertebrae.segmentedImageSelector.setCurrentNode(segmentedImage)
    sctLabelVertebrae.onSelectSegmentedImage()
    params = sctLabelVertebrae.getParams()
    labelVertebraeLogic = SCTLabelVertebrae.SCTLabelVertebraeLogic()
    statusCode = labelVertebraeLogic.run(params)

    labeledImage = None

    if statusCode == 0:
      labeledImage = slicer.mrmlScene.GetNodesByName(segmentedImage.GetName() +
                                                     "_labeled").GetItemAsObject(0)

    return sctLabelVertebrae.statusDict[statusCode], labeledImage

  def labelUtils(self, imageSegLabeled, sctLabelUtils):
    """calls sctLabelUtils and obtains labels for the points given
    returns the status from SCTLabelUtils and the output labels file
    """
    sctLabelUtils.inputImageSelector.setCurrentNode(imageSegLabeled)
    sctLabelUtils.onSelectInputImage()
    params = sctLabelUtils.getParams()
    labelUtilsLogic = SCTLabelUtils.SCTLabelUtilsLogic()
    statusCode = labelUtilsLogic.run(params)

    labels = None

    if statusCode == 0:
      labels = slicer.mrmlScene.GetNodesByName("labels").GetItemAsObject(0)

    return sctLabelUtils.statusDict[statusCode], labels

  def registerToTemplate(self, anatomicalImage, segmentedImage, labels, sctRegisterToTemplate):
    """calls SCT Register To Template
    returns status and a tuple containing the warp_temp2anat
    field at index 0 and warp_anat2tempField at index 1
    """
    sctRegisterToTemplate.anatomicalImageSelector.setCurrentNode(anatomicalImage)
    sctRegisterToTemplate.onSelectAnatomicalImage()
    sctRegisterToTemplate.segmentedImageSelector.setCurrentNode(segmentedImage)
    sctRegisterToTemplate.onSelectSegmentedImage()
    sctRegisterToTemplate.labelsFileSelector.setCurrentNode(labels)

    params = sctRegisterToTemplate.getParams()
    registerToTemplateLogic = SCTRegisterToTemplate.SCTRegisterToTemplateLogic()

    statusCode = registerToTemplateLogic.run(params)

    if statusCode == 0:

      warp_anat2temp = slicer.mrmlScene.GetNodesByName("warp_anat2template").GetItemAsObject(0)
      warp_temp2anat = slicer.mrmlScene.GetNodesByName("warp_template2anat").GetItemAsObject(0)

      return statusCode, (warp_temp2anat, warp_anat2temp)

    return sctRegisterToTemplate.statusDict[statusCode], None


  def warpTemplate(self, destinationImage, warpingField, sctWarpTemplate):
    """calls SCTWarpTemplate module
    returns only the status of SCT Warp Template
    """
    sctWarpTemplate.destinationImageSelector.setCurrentNode(destinationImage)
    sctWarpTemplate.onSelectDestinationImage()
    sctWarpTemplate.warpingFieldSelector.setCurrentNode(warpingField)
    sctWarpTemplate.onSelectWarpingField()
    params = sctWarpTemplate.getParams()
    warpTemplateLogic = SCTWarpTemplate.SCTWarpTemplateLogic()
    statusCode = warpTemplateLogic.run(params)

    return statusCode, sctWarpTemplate.statusDict[statusCode]

  def dmriSeparateB0DWI(self, sctDMRISeparateB0DWI):
    """Calls SCTDMRISeparateb0anddwi
    returns the status from the command and the dwi_mean
    """
    sctDMRISeparateB0DWI.onSelectDiffusionData() # this function automatically loads the bvecs file
    if not sctDMRISeparateB0DWI.bvecsSelector.currentPath:
      return sctDMRISeparateB0DWI.statusDict[-2]
    sctDMRISeparateB0DWI.onSelectBvecs()

    params = sctDMRISeparateB0DWI.getParams()
    dmriSeparateB0DWILogic = SCTDMRISeparateB0DWI.SCTDMRISeparateB0DWILogic()
    statusCode = dmriSeparateB0DWILogic.run(params)

    dwi_mean = None

    if statusCode == 0:
      dwi_mean = slicer.mrmlScene.GetNodesByName("dwi_mean").GetItemAsObject(0)

    return sctDMRISeparateB0DWI.statusDict[statusCode], dwi_mean

  def registerMultimodal(self, sourceImage, destImage, segSource, segDest, initWarp,
                         sctRegisterMultimodal):
    """Calls SCTRegisterMultiModal module
    """

    sctRegisterMultimodal.destinationImageSelector.setCurrentNode(destImage)
    sctRegisterMultimodal.sourceImageSelector.setCurrentNode(sourceImage)
    sctRegisterMultimodal.onSelectMandatoryOption()
    sctRegisterMultimodal.segmentationSourceSelector.setCurrentNode(segSource)
    sctRegisterMultimodal.segmentationDestinationSelector.setCurrentNode(segDest)
    sctRegisterMultimodal.initWarpSelector.setCurrentNode(initWarp)

    # set output file name
    sourceImageName = sourceImage.GetStorageNode().GetFileName()
    outputDir = sourceImageName[:sourceImageName.rfind("/") + 1]
    sctRegisterMultimodal.outputFileSelector.currentPath = outputDir

    params = sctRegisterMultimodal.getParams()
    registerMultimodalLogic = SCTRegisterMultimodal.SCTRegisterMultimodalLogic()
    statusCode = registerMultimodalLogic.run(params)

    outputWarpNode = None

    if statusCode == 0:
      outputwarp = "warp_" + sourceImage.GetName() + "2" + destImage.GetName()
      outputWarpNode = slicer.mrmlScene.GetNodesByName(outputwarp).GetItemAsObject(0)

    return sctRegisterMultimodal.statusDict[statusCode], outputWarpNode

  def concatTransfo(self, destImage, warpImages, sctConcatTransfo):
    """Calls SCTConcatTransfo
    """
    sctConcatTransfo.destinationImageSelector.setCurrentNode(destImage)

    sctConcatTransfo.numberOfImagesSelector.value = len(warpImages)
    sctConcatTransfo.changeNumImages()

    for i in range(len(warpImages)):
      sctConcatTransfo.warpImageSelectors[i].setCurrentNode(warpImages[i])

    # would like to save output warp in dmri directory with default name
    outputWarpDefault = "warp_dmri2anat.nii.gz"

    dmriWarpName = warpImages[0].GetStorageNode().GetFileName()
    dmriPath = dmriWarpName[:dmriWarpName.rfind("/") + 1]

    print("dmriWarpName: " + str(dmriWarpName))

    sctConcatTransfo.outputWarpFieldSelector.currentPath = dmriPath + \
                                                           outputWarpDefault

    params = sctConcatTransfo.getParams()
    concatTransfoLogic = SCTConcatTransfo.SCTConcatTransfoLogic()
    statusCode = concatTransfoLogic.run(params)

    return statusCode, sctConcatTransfo.statusDict[statusCode]

  def runTractographyPipeline(self, inputJSON):
    tractographyPipeline = slicer.modules.tractographypipeline.\
                           createNewWidgetRepresentation().self()
    logic = TractographyPipeline.TractographyPipelineLogic()
    status, outputs = logic.run(inputJSON)
    print status
    print outputs
    return status, outputs

  def getSCTPath(self):
    '''Finds sct/bin directory in system path and adds sct/scripts to system path variable
    returns True if sct/bin was found in system path, else returns false'''

    paths = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          sct_path =  sct_bin_path[:sct_bin_path.rfind("/") + 1]
          return sct_path
      return None
    except KeyError:  # path variable not found
      return None

  def loadNodeFromSceneOrFile(self, name, fullpath):
    """Loads node from scene by the name given, or loads
    the node by the fullpath given. This function should be used when the node in the scene
    and the node that exists in the file are considered to be "equivalent"
    returns a boolean indicating if the volume was successfully loaded (true if success)
    and the volume itself (none if the loading was not successful)
    """

    volume = slicer.util.getNode(name)

    if not volume:
      success, volume = slicer.util.loadVolume(fullpath, {}, True)

    return volume is not None, volume


class SCTTractographyPipelineTest(ScriptedLoadableModuleTest):

  def setUp(self):
    logic = SCTTractographyPipelineLogic

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_SCTTractographyPipeline()

  def test_SCTTractographyPipeline(self):
    """Calls sct_testing to carry out the testing
    """
    self.add_sct_scripts_to_path()
    import sct_testing
    try:
      sct_testing.main(["-f", "sct_propseg"])
      self.delayDisplay('Successfully ran sct_testing')
    except:
      self.delayDisplay("Error running sct_testing")
      print(sys.exc_info())

  def add_sct_scripts_to_path(self):
    """Finds sct/bin directory in system path and adds sct/scripts to system path variable
    returns True if sct/bin was found in system path, else returns false"""

    paths = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          sct_scripts_path =  sct_bin_path[:sct_bin_path.rfind("/") + 1] + "scripts"
          sys.path.append(sct_scripts_path)
          return True
      return True
    except KeyError:  # path variable not found
      return False
