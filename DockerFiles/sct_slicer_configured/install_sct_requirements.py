import pip

conda_requirements = """numpy==1.11.2
matplotlib==1.5.1
scikit-learn==0.17.1
scikit-image==0.12.3
pandas==0.18.1
xlwt==1.0.0
xlrd==0.9.4
xlutils==1.7.1
requests==2.12.4
"""

pip_requirements = """nipy==0.4.1
nibabel==2.1.0
dipy==0.11.0
tqdm==4.11.2
scipy==1.0.0
"""

requirements_list =  conda_requirements.splitlines() + pip_requirements.splitlines()


def main():
   # first install pip requirements
    for req in requirements_list:
        pip_install(req)

    # next install conda requirements
    # for req in conda_requirements.splitlines():
    #     conda_install(req)


def pip_install(module):
    pip.main(["install", module])


if __name__ == "__main__":
    main()
