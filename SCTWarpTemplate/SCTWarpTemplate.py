import sys
import os
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import SimpleITK as sitk
import sitkUtils

#
# SCTWarpTemplate
#

class SCTWarpTemplate(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "SCT Warp Template"
    self.parent.categories = ["Spinal Cord Toolbox"]
    self.parent.contributors = ["Vignesh Sivan (Sunnybrook)"]
    self.parent.helpText = "This function warps the template and all atlases to a given image (e.g. fMRI, DTI, MTR, etc.)."
    self.parent.acknowledgementText = ""

#
# SCTWarpTemplateWidget
#

class SCTWarpTemplateWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):

    ScriptedLoadableModuleWidget.setup(self)

    self.statusDict = {}
    self.statusDict[0] = "Status: SCT Warp Template ran successfully"
    self.statusDict[1] = "Status: Could not load output files from SCT Warp Template"
    self.statusDict[-1] = "Status: SCT Warp Template did not run because sct/scripts directory was not found"
    self.statusDict[-2] = "Status: SCT Warp Template threw some exception"

    # default values
    self.outputFolderDefault = "label/"
    self.templateFolderDefault = self.findTemplateFolder()

    #
    # Main Options
    #

    mandatoryOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    mandatoryOptionsCollapsibleButton.text = "Mandatory Options"
    self.layout.addWidget(mandatoryOptionsCollapsibleButton)

    mandatoryOptionsLayout = qt.QFormLayout(mandatoryOptionsCollapsibleButton)

    self.destinationImageSelector = slicer.qMRMLNodeComboBox()
    self.destinationImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.destinationImageSelector.selectNodeUponCreation = True
    self.destinationImageSelector.addEnabled = False
    self.destinationImageSelector.removeEnabled = True
    self.destinationImageSelector.noneEnabled = True
    self.destinationImageSelector.showHidden = False
    self.destinationImageSelector.showChildNodeTypes = False
    self.destinationImageSelector.setMRMLScene( slicer.mrmlScene )

    mandatoryOptionsLayout.addRow("Destination image", self.destinationImageSelector)

    self.warpingFieldSelector = slicer.qMRMLNodeComboBox()
    self.warpingFieldSelector.nodeTypes = ["vtkMRMLVectorVolumeNode"]
    self.warpingFieldSelector.selectNodeUponCreation = True
    self.warpingFieldSelector.addEnabled = False
    self.warpingFieldSelector.removeEnabled = True
    self.warpingFieldSelector.noneEnabled = True
    self.warpingFieldSelector.showHidden = False
    self.warpingFieldSelector.showChildNodeTypes = False
    self.warpingFieldSelector.setMRMLScene( slicer.mrmlScene )
    mandatoryOptionsLayout.addRow("Warping field", self.warpingFieldSelector)

    #
    # Optional Arguments
    #

    optionalArgumentsCollapsibleButton = ctk.ctkCollapsibleButton()
    optionalArgumentsCollapsibleButton.text = "Optional Arguments"
    self.layout.addWidget(optionalArgumentsCollapsibleButton)

    optionalArgumentsLayout = qt.QFormLayout(optionalArgumentsCollapsibleButton)

    self.warpatlasSelector = qt.QCheckBox()
    self.warpatlasSelector.setChecked(True)
    optionalArgumentsLayout.addRow("Warp atlas", self.warpatlasSelector)

    self.warpspinalcordSelector = qt.QCheckBox()
    self.warpspinalcordSelector.setChecked(True)
    optionalArgumentsLayout.addRow("Warp spinal cord", self.warpspinalcordSelector)

    self.outputFolderSelector = ctk.ctkPathLineEdit()
    self.outputFolderSelector.filters = ctk.ctkPathLineEdit.Dirs
    optionalArgumentsLayout.addRow("Output folder", self.outputFolderSelector)

    self.templateFolderSelector = ctk.ctkPathLineEdit()
    self.templateFolderSelector.filters = ctk.ctkPathLineEdit.Dirs
    self.templateFolderSelector.currentPath = self.templateFolderDefault
    optionalArgumentsLayout.addRow("Template folder", self.templateFolderSelector)

    self.qcSelector = ctk.ctkPathLineEdit()
    self.qcSelector.filters = ctk.ctkPathLineEdit.Dirs

    optionalArgumentsLayout.addRow("Path to QC report", self.qcSelector)

    #
    # Run button
    #

    self.runButton = qt.QPushButton("Run SCT Warp Template")
    self.runButton.enabled = False

    self.resetButton = qt.QPushButton("Reset values")

    self.buttonLayout = qt.QHBoxLayout()
    # self.buttonLayout.addWidget(self.resetButton)
    self.buttonLayout.addWidget(self.runButton)

    self.layout.addLayout(self.buttonLayout)

    self.statusLabel = qt.QLabel("Status: ")
    self.layout.addWidget(self.statusLabel)

    #
    # Connections
    #

    self.destinationImageSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelectDestinationImage)
    self.warpingFieldSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelectWarpingField)
    self.runButton.connect('clicked(bool)', self.onRunButton)

  def findTemplateFolder(self):
    template_folder = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          break
      template_folder =  sct_bin_path[:sct_bin_path.rfind("/")] + "/data/PAM50/"
    except KeyError:  # path variable not found
      pass
    return template_folder

  def onSelectDestinationImage(self):
    destinationImage = self.destinationImageSelector.currentNode()

    if destinationImage:

      destinationImagePath = destinationImage.GetStorageNode().GetFileName()
      destinatinoImageDir = destinationImagePath[:destinationImagePath.rfind("/") + 1]

      if not self.outputFolderSelector.currentPath:
        self.outputFolderSelector.currentPath = destinatinoImageDir + self.outputFolderDefault

      self.runButton.enabled = self.warpingFieldSelector.currentNode()

  def onSelectWarpingField(self):
    warpingField = self.warpingFieldSelector.currentNode()
    if warpingField:
      self.runButton.enabled = self.destinationImageSelector.currentNode()

  def getParams(self):
    # Append mandatory options
    params = {}
    params["-d"] = self.destinationImageSelector.currentNode().GetStorageNode().GetFileName()
    params["-w"] = self.warpingFieldSelector.currentNode().GetStorageNode().GetFileName()

    # Append optional options
    if not self.warpatlasSelector.isChecked():
      params["-a"] = 0
    if not self.warpspinalcordSelector.isChecked():
      params["-s"] = 1
    if self.templateFolderSelector.currentPath:
      params["-t"] = self.templateFolderSelector.currentPath
    if self.outputFolderSelector.currentPath:
      params["-ofolder"] = self.outputFolderSelector.currentPath
    if self.qcSelector.currentPath:
      params["-qc"]  = self.qcSelector.currentPath
    # enforce verbosity
    params["-v"] = 1

    return params

  def onRunButton(self):
    logic = SCTWarpTemplateLogic()
    params = self.getParams()
    statusCode = logic.run(params)
    self.statusLabel.text = self.statusDict[statusCode]

  def cleanup(self):
    pass

#
# SCTWarpTemplateLogic
#

class SCTWarpTemplateLogic(ScriptedLoadableModuleLogic):

  def run(self, params_dict):
    """calls sct_utils.run to run sct_warp_template
    Adds sct_scripts to path by looking at bash variables
    Returns status code:
    statusCode == 0  : sct_warp_template ran without any error
    statusCode == 1  : sct_warp_template ran successfully but could not load output
    statusCode == -1 : sct_warp_template did not run because sct/scripts dir was not found
    statusCode == -2 : sct_warp_template ran and threw some exception
    """

    statusCode = 0

    try:
      if self.addSCTScriptsToPath():

        import sct_utils as sct
        import sct_warp_template
        from msct_parser import Parser

        params_str = Parser.dictionary_to_string(sct_warp_template.get_parser(), params_dict)
        sct.run("sct_warp_template " + params_str)

      else:
        statusCode = -1
    except: # Prevent Slicer from crashing
      statusCode = -2
      print(sys.exc_info())

    return statusCode

  def removeNodeIfExists(self, nodeName):
    node = slicer.util.getNode(nodeName)
    if node:
      slicer.mrmlScene.RemoveNode(node)

  def addSCTScriptsToPath(self):
    '''Finds sct/bin directory in system path and adds sct/scripts to system path variable
    returns True if sct/bin was found in system path, else returns false'''

    paths = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          sct_scripts_path =  sct_bin_path[:sct_bin_path.rfind("/")] + "/scripts"
          sys.path.append(sct_scripts_path)
          return True
      return False
    except KeyError:  # path variable not found
      return False

#
# SCTWarpTemplateTest
#

class SCTWarpTemplateTest(ScriptedLoadableModuleTest):

  def setUp(self):
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_SCTWarpTemplate()

  def test_SCTWarpTemplate(self):
    """Calls sct_testing to carry out the testing
    """
    self.delayDisplay("Error running sct_testing")
