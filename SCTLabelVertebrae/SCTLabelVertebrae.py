import sys
import os
import json
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import SimpleITK as sitk
import sitkUtils

#
# SCTLabelVertebrae
#

class SCTLabelVertebrae(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
 """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "SCT Label Vertebrae"
    self.parent.categories = ["Spinal Cord Toolbox"]
    self.parent.dependencies = []
    self.parent.contributors = ["Vignesh Sivan (Sunnybrook)"]
    self.parent.helpText = """This module takes an anatomical image and its cord segmentation (binary file), and outputs the cord segmentation labeled with vertebral level. The algorithm requires an initialization (first disc) and then performs a disc search in the superior, then inferior direction, using template disc matching based on mutual information score.This function takes an anatomical image and its cord segmentation (binary file), and outputs the cord segmentation labeled with vertebral level. The algorithm requires an initialization (first disc) and then performs a disc search in the superior, then inferior direction, using template disc matching based on mutual information score.

<p> For more information visit the <a href="https://sourceforge.net/p/spinalcordtoolbox/wiki/sct_label_vertebrae/"> spinalcordtoolbox documentation </a> </p>"""


    self.parent.helpText += ""
    self.parent.acknowledgementText = ""

#
# SCTLabelVertebraeWidget
#

class SCTLabelVertebraeWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    self.statusDict = {}
    self.statusDict[0] = "Status: SCT Label Vertebrae ran successfully"
    self.statusDict[1] = "Status: Could not load output files from SCT Label Vertebrae"
    self.statusDict[-1] = "Status: SCT Label Vertebrae did not run because sct \
    directory was not found"
    self.statusDict[-2] = "Status: SCT Label Vertebrae threw some exception"

    #
    # Main Options
    #

    mandatoryOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    mandatoryOptionsCollapsibleButton.text = "Mandatory Options"
    self.layout.addWidget(mandatoryOptionsCollapsibleButton)

    mandatoryOptionsLayout = qt.QFormLayout(mandatoryOptionsCollapsibleButton)

    self.inputImageSelector = slicer.qMRMLNodeComboBox()
    self.inputImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.inputImageSelector.selectNodeUponCreation = True
    self.inputImageSelector.addEnabled = False
    self.inputImageSelector.removeEnabled = True
    self.inputImageSelector.noneEnabled = True
    self.inputImageSelector.showHidden = False
    self.inputImageSelector.showChildNodeTypes = False
    self.inputImageSelector.setMRMLScene( slicer.mrmlScene )

    mandatoryOptionsLayout.addRow("Input image", self.inputImageSelector)

    self.segmentedImageSelector = slicer.qMRMLNodeComboBox()
    self.segmentedImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode", "vtkMRMLLabelMapVolumeNode"]
    self.segmentedImageSelector.selectNodeUponCreation = True
    self.segmentedImageSelector.addEnabled = False
    self.segmentedImageSelector.removeEnabled = True
    self.segmentedImageSelector.noneEnabled = True
    self.segmentedImageSelector.showHidden = False
    self.segmentedImageSelector.showChildNodeTypes = False
    self.segmentedImageSelector.setMRMLScene( slicer.mrmlScene )

    mandatoryOptionsLayout.addRow("Segmented or centerline image", self.segmentedImageSelector)

    self.contrastSelector = qt.QComboBox()
    self.contrastSelector.addItems(['t1','t2'])
    mandatoryOptionsLayout.addRow("Contrast", self.contrastSelector)

    #
    # General Options
    #

    generalOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    generalOptionsCollapsibleButton.text = "General Options"
    self.layout.addWidget(generalOptionsCollapsibleButton)

    generalOptionsLayout = qt.QFormLayout(generalOptionsCollapsibleButton)

    self.templateSelector = slicer.qMRMLNodeComboBox()
    self.templateSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.templateSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.templateSelector.selectNodeUponCreation = True
    self.templateSelector.addEnabled = False
    self.templateSelector.removeEnabled = True
    self.templateSelector.noneEnabled = True
    self.templateSelector.showHidden = False
    self.templateSelector.showChildNodeTypes = False
    self.templateSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Template path", self.templateSelector)

    self.outputFolderSelector = ctk.ctkPathLineEdit()
    self.outputFolderSelector.filters = ctk.ctkPathLineEdit.Dirs
    generalOptionsLayout.addRow("Output folder", self.outputFolderSelector)

    # filters

    filtersGroupBox = qt.QGroupBox("Filters")
    filtersLayout = qt.QFormLayout(filtersGroupBox)

    self.denoiseSelector = qt.QCheckBox()
    filtersLayout.addRow("Apply denoising filter", self.denoiseSelector)

    self.laplaceSelector = qt.QCheckBox()
    filtersLayout.addRow("Apply laplacian filter", self.laplaceSelector)

    generalOptionsLayout.addRow(filtersGroupBox)

    self.removeTempFilesSelector = qt.QCheckBox()
    self.removeTempFilesSelector.setChecked(True)
    generalOptionsLayout.addRow("Remove temporary files", self.removeTempFilesSelector)

    # quality control

    self.noqcSelector = qt.QGroupBox("Generate quality control report")
    self.noqcSelector.setCheckable(True)
    self.noqcSelector.setChecked(False)

    self.qcSelector = ctk.ctkPathLineEdit()
    self.qcSelector.filters = ctk.ctkPathLineEdit.Dirs

    qcLayout = qt.QFormLayout()
    qcLayout.addRow("Path to QC report", self.qcSelector)

    self.noqcSelector.setLayout(qcLayout)

    generalOptionsLayout.addRow(self.noqcSelector)

    # Run button

    self.runButton = qt.QPushButton("Run SCT Label Vertebrae")
    self.runButton.enabled = False

    self.layout.addWidget(self.runButton)

    # Status label
    self.statusLabel = qt.QLabel("Status: ")
    self.layout.addWidget(self.statusLabel)

    #
    # Connections
    #

    self.inputImageSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelectInputImage)
    self.segmentedImageSelector.connect("currentNodeChanged(vtkMRMLNode*)",
                                        self.onSelectSegmentedImage)
    self.runButton.connect('clicked(bool)', self.onRunButton)

  def onSelectInputImage(self):

    inputNode = self.inputImageSelector.currentNode()

    if inputNode:

      inputStorageNode = inputNode.GetStorageNode()
      self.inputPath = inputStorageNode.GetFileName()

      # automatically load segmented or centerline node
      if not self.segmentedImageSelector.currentNode():
        segNode = slicer.mrmlScene.GetNodesByName(inputNode.GetName() + "_seg").GetItemAsObject(0)
        centerlineNode = slicer.mrmlScene.GetNodesByName(inputNode.GetName() + "_centerline").\
        GetItemAsObject(0)
        if segNode:
          self.segmentedImageSelector.setCurrentNode(segNode)
        elif centerlineNode:
          self.segmentedImageSelector.setCurrentNode(centerlineNode)

      # set contrast based on node name
      contrasts = [self.contrastSelector.itemText(i) for i in range(self.contrastSelector.count)]
      for contrast in contrasts:
        if contrast in inputNode.GetName():
          idx = self.contrastSelector.findText(contrast)
          self.contrastSelector.setCurrentIndex(idx)

      # change the output folder to current directory
      self.outputFolderSelector.currentPath = self.inputPath[:self.inputPath.rfind("/")]

      # enable run button
      self.runButton.enabled = self.inputImageSelector.currentNode() and \
                               self.segmentedImageSelector.currentNode()

      # change the status
      self.statusLabel.text = "Status: Successfully imported input image"

  def onSelectSegmentedImage(self):
    # should this also look for the input image, based on name or naa?
    segmentedNode = self.segmentedImageSelector.currentNode()
    if segmentedNode:
      segmentedStorageNode = segmentedNode.GetStorageNode()
      self.segmentedPath = segmentedStorageNode.GetFileName()

      self.runButton.enabled = self.inputImageSelector.currentNode and \
                               self.segmentedImageSelector.currentNode

  def getParams(self):
    # Append mandatory options
    params = {}
    params["-i "] = self.inputPath
    params["-s"] = self.segmentedPath
    params["-c"] = str(self.contrastSelector.currentText)

    # Append general options
    if self.templateSelector.currentNode():
      params["-t"] = self.templateSelector.currentNode().GetStorageNode().GetFileName()
    if self.outputFolderSelector.currentPath:
      params["-ofolder"] = self.outputFolderSelector.currentPath
    if self.denoiseSelector.isChecked():
      params["-denoise"] = ""
    if self.laplaceSelector.isChecked():
      params["-laplacian"] = ""
    if not self.removeTempFilesSelector.isChecked():
      params["-r 0"] = ""
    params["-v"] = 1
    if not self.noqcSelector.isChecked():
      if self.qcSelector.currentPath:
        params["-qc"] = self.qcSelector.currentPath

    return params

  def onRunButton(self):
    logic = SCTLabelVertebraeLogic()
    params = self.getParams()
    statusCode = logic.run(params)
    self.statusLabel.text = self.statusDict[statusCode]

  def cleanup(self):
    pass

#
# SCTLabelVertebraeLogic
#

class SCTLabelVertebraeLogic(ScriptedLoadableModuleLogic):

  def run(self, params_dict):
    """calls sct_utils.run to run command
    Adds sct_scripts to path by looking at bash variables
    Returns status code:
    statusCode == 0  : sct_label_vertebrae ran without any error
    statusCode == 1  : sct_label_vertebrae ran successfully but could not load output
    statusCode == -1 : sct_label_vertebrae did not run because sct/scripts dir was not found
    statusCode == -2 : sct_label_vertebrae ran and threw some exception
    """

    statusCode = 0

    try:

      if self.addSCTScriptsToPath():

        import sct_utils as sct
        import sct_label_vertebrae
        from msct_parser import Parser

        params_str = Parser.dictionary_to_string(sct_label_vertebrae.get_parser(), params_dict)

        sct.run("sct_label_vertebrae " + params_str)

        # load all output files and remove previously loaded volumes with the same name

        outputFolder = params_dict["-ofolder"]
        segmentedPath = params_dict["-s"]
        segmentedFileName = segmentedPath[segmentedPath.rfind("/") + 1:segmentedPath.find(".")]
        outputSegPrefix = outputFolder + segmentedFileName

        if outputFolder[-1] != "/":
          outputFolder += "/"

        segLabeledName = segmentedFileName + "_labeled"
        segLabeledDiscsName = segmentedFileName + "_labeled_discs"

        warpCurve2StraightName = "warp_curve2straight"
        warpStraight2CurveName = "warp_straight2curve"
        straightRefName = "straight_ref"

        self.removeNodeIfExists(segLabeledName)
        self.removeNodeIfExists(segLabeledDiscsName)
        self.removeNodeIfExists(warpCurve2StraightName)
        self.removeNodeIfExists(warpCurve2StraightName)
        self.removeNodeIfExists(warpStraight2CurveName)


        nii = ".nii.gz"

        segLabeledPath = outputFolder + segLabeledName + nii
        segLabeledDiscsPath = outputFolder + segLabeledDiscsName + nii
        straightRefPath = outputFolder + straightRefName + nii
        warpCurve2StraightPath = outputFolder + warpCurve2StraightName + nii
        warpStraight2CurvePath = outputFolder + warpStraight2CurveName + nii

        load_segLabeled, segLabeled = slicer.util.loadVolume(segLabeledPath,
                                                             {"labelmap": True}, True)

        load_segLabeledDiscs, segLabeledDiscs = slicer.util.loadVolume(segLabeledDiscsPath,
                                                                       {"show": False}, True)

        load_straightRef, straightRef = slicer.util.loadVolume(straightRefPath,
                                                               {"show" : False}, True)

        loadwc2s, wc2s = slicer.util.loadVolume(warpCurve2StraightPath, {"show": False}, True)
        loadws2c, ws2c = slicer.util.loadVolume(warpStraight2CurvePath, {"show" : False}, True)

        if not load_segLabeled: statusCode = 1
        else:
          segLabeled.SetName(segLabeledName)
          # visualize the output
          selectionNode = slicer.app.applicationLogic().GetSelectionNode()
          selectionNode.SetReferenceActiveLabelVolumeID(segLabeled.GetID())

        if not load_segLabeledDiscs: statusCode = 1
        else: segLabeledDiscs.SetName(segLabeledDiscsName)
        if not load_straightRef: statusCode = 1
        else: straightRef.SetName(straightRefName)
        if not loadwc2s: statusCode = 1
        else: wc2s.SetName(warpCurve2StraightName)
        if not loadws2c: statusCode = 1
        else: ws2c.SetName(warpStraight2CurveName)

        # update view
        applicationLogic = slicer.app.applicationLogic()
        applicationLogic.PropagateVolumeSelection(0)

      else:
        statusCode = -1

    except: # Prevent Slicer from crashing
      statusCode = -2
      print(sys.exc_info())

    return statusCode

  def removeNodeIfExists(self, nodeName):
    node = slicer.util.getNode(nodeName)
    if node:
      slicer.mrmlScene.RemoveNode(node)

  def addSCTScriptsToPath(self):
    '''Finds sct/bin directory in system path and adds sct/scripts to system path variable
    returns True if sct/bin was found in system path, else returns false'''

    paths = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          sct_scripts_path =  sct_bin_path[:sct_bin_path.rfind("/")] + "/scripts"
          sys.path.append(sct_scripts_path)
          return True
      return False
    except KeyError:  # path variable not found
      return False

class SCTLabelVertebraeTest(ScriptedLoadableModuleTest):

  def setUp(self):
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_SCTLabelVertebrae()

  def test_SCTLabelVertebrae(self):
    """Calls sct_testing to carry out the testing
    """
    self.delayDisplay('No tests available')
    # TODO
