import sys
import os
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import SimpleITK as sitk
import sitkUtils

#
# SCTRegisterMultimodal
#

class SCTRegisterMultimodal(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
 """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "SCT Register Multimodal"
    self.parent.categories = ["Spinal Cord Toolbox"]
    self.parent.dependencies = []
    self.parent.contributors = ["Vignesh Sivan (Sunnybrook)"]
    self.parent.helpText = """
    <h1>SCTRegisterMultiModal</h1>
    <img src="/usr/local/Vignesh/SCTWrappers/SCTRegisterMultimodal/sct_register_multimodal_Example.png">
<p>This program co-registers two 3D volumes. The deformation is non-rigid and is constrained along Z direction (i.e., axial plane). Hence, this function assumes that orientation of the destination image is axial (RPI). If you need to register two volumes with large deformations and/or different contrasts, it is recommended to input spinal cord segmentations (binary mask) in order to achieve maximum robustness. To do so, you can use sct_segmentation_propagation.</p>
<p>The program outputs a warping field that can be used to register other images to the destination image. To apply the warping field to another image, use sct_apply_transfo.</p>
<p>For more information on how to set the parameters, see the <a href="https://sourceforge.net/p/spinalcordtoolbox/wiki/registration_tricks/">registration tricks.</a><p>"""

    self.parent.helpText += ""
    self.parent.acknowledgementText = ""

#
# SCTRegisterMultimodalWidget
#

class SCTRegisterMultimodalWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    self.statusDict = {}
    self.statusDict[0] = "Status: SCT Register Multimodal ran successfully"
    self.statusDict[1] = "Status: Could not load output files from SCT Register Multimodal"
    self.statusDict[-1] = "Status: SCT Register Multimodal did not run because \
    sct/scripts directory was not found"
    self.statusDict[-2] = "Status: SCT Register Multimodal threw some exception"

    #
    # Main Options
    #

    mandatoryOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    mandatoryOptionsCollapsibleButton.text = "Mandatory Options"
    self.layout.addWidget(mandatoryOptionsCollapsibleButton)

    mandatoryOptionsLayout = qt.QFormLayout(mandatoryOptionsCollapsibleButton)

    self.sourceImageSelector = slicer.qMRMLNodeComboBox()
    self.sourceImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.sourceImageSelector.selectNodeUponCreation = True
    self.sourceImageSelector.addEnabled = False
    self.sourceImageSelector.removeEnabled = True
    self.sourceImageSelector.noneEnabled = True
    self.sourceImageSelector.showHidden = False
    self.sourceImageSelector.showChildNodeTypes = False
    self.sourceImageSelector.setMRMLScene( slicer.mrmlScene )

    mandatoryOptionsLayout.addRow("Image source", self.sourceImageSelector)

    self.destinationImageSelector = slicer.qMRMLNodeComboBox()
    self.destinationImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.destinationImageSelector.selectNodeUponCreation = True
    self.destinationImageSelector.addEnabled = False
    self.destinationImageSelector.removeEnabled = True
    self.destinationImageSelector.noneEnabled = True
    self.destinationImageSelector.showHidden = False
    self.destinationImageSelector.showChildNodeTypes = False
    self.destinationImageSelector.setMRMLScene( slicer.mrmlScene )

    mandatoryOptionsLayout.addRow("Image destination", self.destinationImageSelector)

    #
    # General Options
    #

    generalOptionsCollapsibleButton = ctk.ctkCollapsibleButton()
    generalOptionsCollapsibleButton.text = "General Options"
    self.layout.addWidget(generalOptionsCollapsibleButton)

    generalOptionsLayout = qt.QFormLayout(generalOptionsCollapsibleButton)
    # generalOptionsLayout = qt.QGridLayout(generalOptionsCollapsibleButton)

    self.segmentationSourceSelector = slicer.qMRMLNodeComboBox()

    self.segmentationSourceSelector.nodeTypes = ["vtkMRMLLabelMapVolumeNode",
                                                 "vtkMRMLScalarVolumeNode"]

    self.segmentationSourceSelector.selectNodeUponCreation = True
    self.segmentationSourceSelector.addEnabled = False
    self.segmentationSourceSelector.removeEnabled = True
    self.segmentationSourceSelector.noneEnabled = True
    self.segmentationSourceSelector.showHidden = False
    self.segmentationSourceSelector.showChildNodeTypes = False
    self.segmentationSourceSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Segmentation source", self.segmentationSourceSelector)

    self.segmentationDestinationSelector = slicer.qMRMLNodeComboBox()

    self.segmentationDestinationSelector.nodeTypes = ["vtkMRMLLabelMapVolumeNode",
                                                      "vtkMRMLScalarVolumeNode"]

    self.segmentationDestinationSelector.selectNodeUponCreation = True
    self.segmentationDestinationSelector.addEnabled = False
    self.segmentationDestinationSelector.removeEnabled = True
    self.segmentationDestinationSelector.noneEnabled = True
    self.segmentationDestinationSelector.showHidden = False
    self.segmentationDestinationSelector.showChildNodeTypes = False
    self.segmentationDestinationSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Segmentation destination", self.segmentationDestinationSelector)


    self.labelSourceSelector = slicer.qMRMLNodeComboBox()
    self.labelSourceSelector.nodeTypes = ["vtkMRMLLabelMapVolumeNode"]
    self.labelSourceSelector.selectNodeUponCreation = True
    self.labelSourceSelector.addEnabled = False
    self.labelSourceSelector.removeEnabled = True
    self.labelSourceSelector.noneEnabled = True
    self.labelSourceSelector.showHidden = False
    self.labelSourceSelector.showChildNodeTypes = False
    self.labelSourceSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Labels source", self.labelSourceSelector)

    self.labelDestinationSelector = slicer.qMRMLNodeComboBox()
    self.labelDestinationSelector.nodeTypes = ["vtkMRMLLabelMapVolumeNode"]
    self.labelDestinationSelector.selectNodeUponCreation = True
    self.labelDestinationSelector.addEnabled = False
    self.labelDestinationSelector.removeEnabled = True
    self.labelDestinationSelector.noneEnabled = True
    self.labelDestinationSelector.showHidden = False
    self.labelDestinationSelector.showChildNodeTypes = False
    self.labelDestinationSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Labels destination", self.labelDestinationSelector)

    self.initWarpSelector = slicer.qMRMLNodeComboBox()
    self.initWarpSelector.nodeTypes = ["vtkMRMLVectorVolumeNode"]
    self.initWarpSelector.selectNodeUponCreation = True
    self.initWarpSelector.addEnabled = False
    self.initWarpSelector.removeEnabled = True
    self.initWarpSelector.noneEnabled = True
    self.initWarpSelector.showHidden = False
    self.initWarpSelector.showChildNodeTypes = False
    self.initWarpSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Initial warping field", self.initWarpSelector)

    self.initWarpInvSelector = slicer.qMRMLNodeComboBox()
    self.initWarpInvSelector.nodeTypes = ["vtkMRMLVectorVolumeNode"]
    self.initWarpInvSelector.selectNodeUponCreation = True
    self.initWarpInvSelector.addEnabled = False
    self.initWarpInvSelector.removeEnabled = True
    self.initWarpInvSelector.noneEnabled = True
    self.initWarpInvSelector.showHidden = False
    self.initWarpInvSelector.showChildNodeTypes = False
    self.initWarpInvSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Initial inverse warping", self.initWarpInvSelector)

    self.maskSelector = slicer.qMRMLNodeComboBox()
    self.maskSelector.nodeTypes = ["vtkMRMLVectorVolumeNode"]
    self.maskSelector.selectNodeUponCreation = True
    self.maskSelector.addEnabled = False
    self.maskSelector.removeEnabled = True
    self.maskSelector.noneEnabled = True
    self.maskSelector.showHidden = False
    self.maskSelector.showChildNodeTypes = False
    self.maskSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Mask", self.maskSelector)

    self.outputWarpFieldSelector = slicer.qMRMLNodeComboBox()
    self.outputWarpFieldSelector.nodeTypes = ["vtkMRMLVectorVolumeNode"]
    self.outputWarpFieldSelector.selectNodeUponCreation = True
    self.outputWarpFieldSelector.addEnabled = False
    self.outputWarpFieldSelector.removeEnabled = True
    self.outputWarpFieldSelector.noneEnabled = True
    self.outputWarpFieldSelector.showHidden = False
    self.outputWarpFieldSelector.showChildNodeTypes = False
    self.outputWarpFieldSelector.setMRMLScene( slicer.mrmlScene )

    generalOptionsLayout.addRow("Output warp field", self.outputWarpFieldSelector)


    self.outputFileSelector = ctk.ctkPathLineEdit()
    generalOptionsLayout.addRow("Output file", self.outputFileSelector)

    #
    # Parameters for registration
    #

    registrationParamsCollapsibleButton = ctk.ctkCollapsibleButton()
    registrationParamsCollapsibleButton.text = "Parameters for registration"
    self.layout.addWidget(registrationParamsCollapsibleButton)

    registrationParamsLayout = qt.QFormLayout()

    registrationParamsCollapsibleButton.setLayout(registrationParamsLayout)

    self.paramsForReg = qt.QTextEdit()

    registrationParamsLayout.addRow("Parameters for registration", self.paramsForReg)

    addStepButton = qt.QPushButton("Add new step")
    removeStepButton = qt.QPushButton("Remove last step")

    buttonLayout = qt.QHBoxLayout()

    buttonLayout.addWidget(addStepButton)
    buttonLayout.addWidget(removeStepButton)

    registrationParamsLayout.addRow(buttonLayout)

    #
    # Run button
    #

    self.runButton = qt.QPushButton("Run SCT Register Multimodal")
    self.runButton.enabled = False

    self.layout.addWidget(self.runButton)

    # Status label
    self.statusLabel = qt.QLabel("Status: ")
    self.layout.addWidget(self.statusLabel)

    #
    # Connections
    #

    self.destinationImageSelector.connect("currentNodeChanged(vtkMRMLNode*)",
                                          self.onSelectMandatoryOption)

    self.sourceImageSelector.connect("currentNodeChanged(vtkMRMLNode*)",
                                     self.onSelectMandatoryOption)

    addStepButton.connect('clicked(bool)', self.addParamForReg)
    removeStepButton.connect('clicked(bool)', self.removeLastParamForReg)
    self.runButton.connect('clicked(bool)', self.onRunButton)

  def onSelectMandatoryOption(self):
    '''only checks if mandatory
    '''
    self.runButton.enabled = self.destinationImageSelector.currentNode() and \
                             self.sourceImageSelector.currentNode()

  def addParamForReg(self):
    paramsDialog = ParamsDialog()
    setParams = paramsDialog.exec_()
    if setParams == 1:
      params = paramsDialog.getParams()
      if self.paramsForReg.toPlainText() != "":
        params = ":" + params
      self.paramsForReg.insertPlainText(params)

  def removeLastParamForReg(self):
    paramsText = self.paramsForReg.toPlainText()

    if ":" not in paramsText:  # there aren't multiple steps in this case
      self.paramsForReg.clear()

    else:
      indexOfLastColon = paramsText.rfind(":")
      newText = paramsText[:indexOfLastColon]
      self.paramsForReg.clear()
      self.paramsForReg.insertPlainText(newText)

  def getParams(self):
    # Append mandatory options
    params = {}
    params["-i"] = self.sourceImageSelector.currentNode().GetStorageNode().GetFileName()
    params["-d"] = self.destinationImageSelector.currentNode().GetStorageNode().GetFileName()
    if self.segmentationSourceSelector.currentNode():
      params["-iseg"] = self.segmentationSourceSelector.currentNode().GetStorageNode().GetFileName()
    if self.segmentationDestinationSelector.currentNode():
      params["-dseg"] = self.segmentationDestinationSelector.currentNode().\
                        GetStorageNode().GetFileName()
    if self.labelSourceSelector.currentNode():
      params["-ilabel"] = self.labelSourceSelector.currentNode().GetStorageNode().GetFileName()
    if self.labelDestinationSelector.currentNode():
      params["-dlabel"] = self.labelDestinationSelector.currentNode().GetStorageNode().GetFileName()
    if self.initWarpSelector.currentNode():
      params["-initwarp"] = self.initWarpSelector.currentNode().GetStorageNode().GetFileName()
    if self.initWarpInvSelector.currentNode():
      params["-initwarpinv"] = self.initWarpInvSelector.currentNode().GetStorageNode().GetFileName()
    if self.maskSelector.currentNode():
      params["-m"] = self.maskSelector.currentNode().GetStorageNode().GetFileName()
    if self.outputWarpFieldSelector.currentNode():
      params["-owarp"] = self.outputWarpFieldSelector.currentNode().GetStorageNode().GetFileName()
    if self.outputFileSelector.currentPath:
      params["-ofolder"] = self.outputFileSelector.currentPath
    if self.paramsForReg.plainText:
      params["-param"] = self.paramsForReg.plainText

    return params

  def onRunButton(self):
    logic = SCTRegisterMultimodalLogic()

    params = self.getParams()
    statusCode = logic.run(params)
    self.statusLabel.text = self.statusDict[statusCode]

  def cleanup(self):
    pass

#
# SCTRegisterMultimodalLogic
#

class SCTRegisterMultimodalLogic(ScriptedLoadableModuleLogic):

  def run(self, params_dict):
    """calls sct_utils.run to run sct_register_multimodal
    Adds sct_scripts to path by looking at bash variables
    Returns status code:
    statusCode == 0  : sct_register_multimodal ran without any error
    statusCode == 1  : sct_register_multimodal ran successfully but could not load output
    statusCode == -1 : sct_register_multimodal did not run because sct/scripts dir was not found
    statusCode == -2 : sct_register_multimodal ran and threw some exception
    """

    statusCode = 0

    try:

      if self.add_sct_scripts_to_path():

        import sct_utils as sct
        import sct_register_multimodal
        from msct_parser import Parser

        params_str = Parser.dictionary_to_string(sct_register_multimodal.get_parser(), params_dict)

        sct.run("sct_register_multimodal " + params_str)

        # load the output

        inputImage = params_dict["-i"][params_dict["-i"].rfind("/") + 1:params_dict["-i"].find(".")]
        destImage  = params_dict["-d"][params_dict["-d"].rfind("/") + 1:params_dict["-d"].find(".")]
        outputwarp = "warp_" + inputImage + "2" + destImage

        outputFolder = os.getcwd() + "/"
        if "-ofolder"  in params_dict:
          outputFolder = params_dict["-ofolder"]
        nii = ".nii.gz"

        self.removeNodeIfExists(outputwarp)

        print("outputFolder: " + str(outputFolder))

        load_warp, warp = slicer.util.loadVolume(outputFolder + outputwarp + nii,
                                                 {"show": False}, True)

        print(outputwarp)
        print(outputFolder + outputwarp)
        print(load_warp)

        if not load_warp: statusCode = 1
        else: warp.SetName(outputwarp)

      else:
        statusCode = -1

    except: # Prevent Slicer from crashing
      statusCode = -2
      print(sys.exc_info())

    return statusCode

  def removeNodeIfExists(self, nodeName):
    node = slicer.util.getNode(nodeName)
    if node:
      slicer.mrmlScene.RemoveNode(node)

  def add_sct_scripts_to_path(self):
    '''Finds sct/bin directory in system path and adds sct/scripts to system path variable
    returns True if sct/bin was found in system path, else returns false'''

    paths = ""
    try:
      paths = os.environ["PATH"].split(":")
      sct_bin_path = ""
      for path in paths:
        if "sct/bin" in path:
          sct_bin_path = path
          sct_scripts_path =  sct_bin_path[:sct_bin_path.rfind("/")] + "/scripts"
          sys.path.append(sct_scripts_path)
          return True
      return False
    except KeyError:  # path variable not found
      return False

class SCTRegisterMultimodalTest(ScriptedLoadableModuleTest):

  def setUp(self):
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_SCTRegisterMultimodal()

  def test_SCTRegisterMultimodal(self):
    """Calls sct_testing to carry out the testing
    """
    self.delayDisplay('No tests available')

class ParamsDialog (qt.QDialog):

  def __init__(self):
    qt.QGroupBox.__init__(self)
    self.setWindowTitle("Set Parameters for Reg")


    types = ["im", "seg", "label"]
    algos = ["translation", "rigid", "affine", "syn", "bsplinesyn",
             "slicereg", "centermass", "centermassrot", "columnwise"]
    initTranslations = ["geometric", "centermass", "origin"]
    metrics = ["CC", "MI", "MeanSquares"]


    # Default values

    self.algosDefault        = "syn"
    self.metricDefault       = "MeanSquares"
    self.iterDefault         = 10
    self.shrinkDefault       = 1
    self.smoothDefault       = 0
    self.gradStepDefault     = 0.5
    self.deformationDefault  = "1x1x0"
    self.polyDefault         = 5
    self.slicewiseDefault    = 0
    self.laplacianDefault    = 0
    self.dofDefault          = "Tx_Ty_Tz_Rx_Ry_Rz"
    self.smoothDefault       = 2
    self.pcaEigenRatDefault  = 1.6
    self.smoothWarpXYDefault = 2

    self.stepSelector = qt.QSpinBox()
    self.stepSelector.value = 1

    self.typeSelector = qt.QComboBox()
    self.typeSelector.addItems(types)
    self.typeSelector.model().item(2).setEnabled(False)

    self.algoSelector = qt.QComboBox()
    self.algoSelector.addItems(algos)
    self.algoSelector.currentIndex = algos.index(self.algosDefault)

    self.slicewiseSelector = qt.QSpinBox()
    self.slicewiseSelector.value = self.slicewiseDefault

    self.metricSelector = qt.QComboBox()
    self.metricSelector.addItems(metrics)
    self.metricSelector.currentIndex = metrics.index(self.metricDefault)

    self.numItersSelector = qt.QSpinBox()
    self.numItersSelector.value = self.iterDefault

    self.shrinkSelector = qt.QSpinBox()
    self.shrinkSelector.value = self.shrinkDefault

    self.smoothSelector = qt.QSpinBox()
    self.smoothSelector.value = self.smoothDefault

    self.laplacianSelector = qt.QSpinBox()
    self.laplacianSelector.value = self.laplacianDefault

    self.gradStepSelector = qt.QDoubleSpinBox()
    self.gradStepSelector.value = self.gradStepDefault

    self.deformationSelector = qt.QLineEdit()
    self.deformationSelector.setText(self.deformationDefault)

    self.initSelector = qt.QComboBox()
    self.initSelector.addItems(initTranslations)

    self.polySelector = qt.QSpinBox()
    self.polySelector.value = self.polyDefault

    self.smoothWarpXYSelector = qt.QSpinBox()
    self.smoothWarpXYSelector.setEnabled(False)
    self.smoothWarpXYSelector.value = self.smoothWarpXYDefault

    self.pcaEigenRatSelector = qt.QDoubleSpinBox()
    self.pcaEigenRatSelector.value = self.pcaEigenRatDefault
    self.pcaEigenRatSelector.setEnabled(False)

    self.dofSelector = qt.QLineEdit()
    self.dofSelector.setText(self.dofDefault)

    dialogLayout = qt.QVBoxLayout()
    self.formLayout = qt.QFormLayout()

    self.formLayout.addRow("Step", self.stepSelector)
    self.formLayout.addRow("Type", self.typeSelector)
    self.formLayout.addRow("Algorithm", self.algoSelector)
    self.formLayout.addRow("Metric", self.metricSelector)
    self.formLayout.addRow("Number of iterations", self.numItersSelector)
    self.formLayout.addRow("Shrink", self.shrinkSelector)
    self.formLayout.addRow("Smooth", self.smoothSelector)
    self.formLayout.addRow("Laplacian", self.laplacianSelector)
    self.formLayout.addRow("Grad Step", self.gradStepSelector)
    self.formLayout.addRow("Deformation", self.deformationSelector)
    self.formLayout.addRow("Slicewise", self.slicewiseSelector)
    self.formLayout.addRow("init", self.initSelector)
    self.formLayout.addRow("poly", self.polySelector)
    self.formLayout.addRow("Degrees of freedom", self.dofSelector)
    self.formLayout.addRow("Smooth Warp XY", self.smoothWarpXYSelector)
    self.formLayout.addRow("PCA Eigenvalue Ratio", self.pcaEigenRatSelector)

    setParamsBttn = qt.QPushButton("Set parameters")
    closeBttn = qt.QPushButton("Close")

    bttnLayout = qt.QHBoxLayout()
    bttnLayout.addWidget(setParamsBttn)
    bttnLayout.addWidget(closeBttn)

    formGroupBox = qt.QGroupBox()
    formGroupBox.setLayout(self.formLayout)

    dialogLayout.addWidget(formGroupBox)
    dialogLayout.addLayout(bttnLayout)

    self.setLayout(dialogLayout)

    # connections
    self.stepSelector.connect('valueChanged(int)', self.stepSelectorChanged)
    self.typeSelector.currentIndexChanged.connect(self.typeChanged)
    self.algoSelector.currentIndexChanged.connect(self.algoChanged)
    setParamsBttn.connect('clicked(bool)', self.setParams)
    closeBttn.connect('clicked(bool)', self.close)

    # initialize form
    self.stepSelectorChanged()
    self.typeChanged()
    self.algoChanged()

  def stepSelectorChanged(self):
    """Checks if the step is 0. If the step is 0, the user can select type=label
    """
    labelEnabled = self.stepSelector.value == 0
    self.typeSelector.model().item(2).setEnabled(labelEnabled)

  def typeChanged(self):
    """Checks if type is label and if type is label, enable dof selector
    """
    labelIndex = 2
    isLabel = self.typeSelector.currentIndex == labelIndex
    if isLabel:
      self.dofSelector.text = self.dofDefault
      self.dofSelector.setEnabled(isLabel)

  def algoChanged(self):
    """Checks if the algo is slicereg,columnwise or centermassrot and enables/disables
    fields accordingly
    """
    sliceregIndex = 5
    centerMassRotIndex = 7
    columnwiseIndex = 8
    isSlicereg = self.algoSelector.currentIndex == sliceregIndex
    isCenterMassRot = self.algoSelector.currentIndex == centerMassRotIndex
    isColumnWise = self.algoSelector.currentIndex == columnwiseIndex

    if isSlicereg:
      self.polySelector.setEnabled(True)
    else:
      self.polySelector.setEnabled(False)
      self.polySelector.value = self.polyDefault

    if isCenterMassRot:
      self.pcaEigenRatSelector.setEnabled(True)
    else:
      self.pcaEigenRatSelector.setEnabled(False)
      self.pcaEigenRatSelector.value = self.pcaEigenRatDefault

    if isColumnWise:
      self.smoothWarpXYSelector.setEnabled(True)
    else:
      self.smoothWarpXYSelector.setEnabled(False)
      self.smoothWarpXYSelector.value = self.smoothWarpXYDefault

  def getParams(self):
    """Collects values from fields and appends them to a paramter string
    """

    params_str = ""
    params_str += "step=" + str(self.stepSelector.value) + ","
    params_str += "type=" + self.getTextFromComboBox(self.typeSelector) + ","
    params_str += "algo=" + self.getTextFromComboBox(self.algoSelector)

    if self.getTextFromComboBox(self.metricSelector) != self.metricDefault:
      params_str += ",metric=" + self.getTextFromComboBox(self.metricSelector)
    if self.smoothSelector.value != self.smoothDefault:
      params_str += ",smooth=" + str(self.smoothSelector.value)
    if self.shrinkSelector.value != self.shrinkDefault:
      params_str += ",shrink=" + str(self.shrinkSelector.value)
    if self.laplacianSelector.value != self.laplacianDefault:
      params_str += ",laplacian=" + str(self.laplacianSelector.value)
    if self.gradStepSelector.value != self.gradStepDefault:
      params_str += ",gradStep=" + str(self.gradStepSelector.value)
    if str(self.deformationSelector.text) != self.deformationDefault:
      params_str += ",deformation" + self.deformationSelector.text()
    params_str += ",init=" + self.getTextFromComboBox(self.initSelector)
    if self.polySelector.value != self.polyDefault:
      params_str += ",poly=" + str(self.polySelector.value)
    if self.dofSelector.text != self.dofDefault:
      params_str += ",dof=" + self.dofSelector.text
    if self.smoothWarpXYSelector.value != self.smoothWarpXYDefault:
      params_str += ",smoothWarpXY=" + str(self.smoothWarpXYSelector.value)
    if self.pcaEigenRatSelector.value != self.pcaEigenRatDefault:
      params_str += ",pca_eigenratio_th=" + str(self.pcaEigenRatSelector.vaule)

    return params_str

  def getTextFromComboBox(self, comboBox):
    """Gets text from combo box (avoid tedious call to item text and current index)
    """
    return comboBox.itemText(comboBox.currentIndex)

  def setParams(self):
    self.close(returnval=1)

  def close(self, returnval=0):
    self.done(returnval)
